<?php
    
    /**
     * Laravel IoC bindings
     */
    
    App::bind('geocoder', function() {
        $geocoder = new \Geocoder\Geocoder();
        $adapter  = new \Geocoder\HttpAdapter\GuzzleHttpAdapter();
        $geocoder->registerProviders(array(
            new \Geocoder\Provider\GoogleMapsProvider(
                $adapter, 'en_GB', 'United Kingdom', false
            ),
        ));

        return $geocoder;
    });