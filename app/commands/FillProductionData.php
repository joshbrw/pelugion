<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FillProductionData extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'data:fill';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Fill data for production.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		Eloquent::unguard();
		User::truncate();
		Instrument::truncate();
		Page::truncate();
		ContentSection::truncate();

		$this->info('Creating users...');
		$this->createUsers();

		$this->info('');

		$this->info('Creating instruments...');
		$this->createInstruments();

		$this->info('');
		$this->info('Seeding content...');
		$this->createPagesAndContent();
	}

	public function createUsers() {
		User::truncate();

		User::create([
			'first_name' => 'Josh',
			'last_name' => 'Brown',
			'email' => 'josh@joshbrown.me',
			'password' => Hash::make('QWJwkuT4fyz9X9fd2'),
			'date_of_birth' => Time::parse('9th march 1995'),
			'role' => 1,
			'band_member' => false
		]);
		$this->info('- Josh Brown created');

		User::create([
			'first_name' => 'Brandon',
			'last_name' => 'Balou',
			'nickname' => 'The Vulture',
			'email' => 'brandonbalou@gmail.com',
			'password' => Hash::make('Q3n9XAFeU9XzZsfM9'),
			'date_of_birth' => Time::parse('1st january 1970'),
			'role' => 0,
			'band_member' => true
		]);
		$this->info('- Brandon Balou created');

		User::create([
			'first_name' => 'Andy',
			'last_name' => 'Sweeney',
			'nickname' => 'The Quatch',
			'email' => 'andy_pandy2k6@hotmail.com',
			'password' => Hash::make('MRRnZxU6t7HRv8p8D'),
			'date_of_birth' => Time::parse('1st january 1970'),
			'role' => 0,
			'band_member' => true
		]);
		$this->info('- Andy Sweeney created');

		User::create([
			'first_name' => 'John',
			'last_name' => 'Pittaway',
			'nickname' => 'The Goat',
			'email' => 'j.pittaway95@gmail.com',
			'password' => Hash::make('KFEC8cPQkH32x3suA'),
			'date_of_birth' => Time::parse('1st january 1970'),
			'role' => 0,
			'band_member' => true
		]);
		$this->info('- John Pittaway created');
	}

	public function createInstruments() {
		Instrument::truncate();

		$instruments = array('Vocals', 'Guitar', 'Drums', 'Bass');

		foreach($instruments as $instrument) {
			$i = new Instrument;
			$i->name = $instrument;
			$i->save();

			$this->info('- '.$instrument. ' created');
		}
	}

	public function createPagesAndContent() {
		Page::truncate();
		ContentSection::truncate(); 
		$content = [
			[
				'name' => 'Page Template',
				'description' => 'The main template of the page',
				'identifier' => 'template',
				'content' => [
					[
						'identifier' => 'contact-details',
						'description' => 'These are the contact details displayed on the footer of each page.',
						'title' => 'Contact Details',
						'content' => "27 Whatever Wherever<br />Wherever<br />Coventry<br />CV2 666<br />02476 666 123<br />contact@pelugion.com",
					]
				],
			],
			[
				'name' => 'About Page',
				'description' => 'The main about page on the website',
				'identifier' => 'about',
				'content' => [
					[
						'identifier' => 'main',
						'description' => 'Who are Pelugion? Talk about who you are and your past/future.',
						'title' => 'About',
						'content' => '<p>Pelugion are a heavy metal band from Coventry</p>',
					]
				],
			],
			[
				'name' => 'Contact Page',
				'description' => 'The contact page on the website.',
				'identifier' => 'contact',
				'content' => [
					[
						'identifier' => 'main',
						'description' => 'This is displayed above the contact form.',
						'title' => 'Contact Description',
						'content' => '<p>Use the form below to contact Pelugion.</p>'
					],
					[
						'identifier' => 'success',
						'description' => 'This is displayed when the form is successfully submitted.',
						'title' => 'Contact Success Message',
						'content' => '<p>Thank you for your message! Pelugion will get back to you as soon as possible.</p>'
					]
				]
			]
		];

		foreach($content as $pageData) {

			$page = Page::create([
				'name' => $pageData['name'],
				'description' => $pageData['description'],
				'identifier' => $pageData['identifier'],
			]);


			foreach($pageData['content'] as $contentSection) {
				ContentSection::create([
					'title' => $contentSection['title'],
					'content' => $contentSection['content'],
					'description' => $contentSection['description'],
					'identifier' => $contentSection['identifier'],
					'page_id' => $page->id
				]);
			}

		}

	}
}
