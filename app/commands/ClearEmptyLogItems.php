<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ClearEmptyLogItems extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'log:clear';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Clear log items of things that have been deleted.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		foreach(LogItem::all() as $item) {
			/**
			 * If the object isn't found, delete the log item as it will error in placez
			 */
			if(!$item->object) $item->delete();
		}
	}


}
