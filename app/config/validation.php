<?php

	return [
		'account.update' => [
			'first_name' => [ 'required', 'max:50' ],
			'last_name' => [ 'required', 'max:50' ],
			'nickname' => [ 'max:50' ],
			'email' => [ 'required', 'email']
		],
		'forgotten.submit' => [
			'email' => [ 'required', 'exists:users,email' ]
		],
		'news.create' => [
			'title' => [ 'required', 'between:5,255' ],
			'subtext' => [ 'required', 'between:10,100' ],
			'body' => [ 'required', 'between:20,10000' ]
		],
		'avatar.update' => [
			'profile_image' => [ 'required', 'image', 'max:2048' ]
		],
		'password.update' => [
			'current_password' => [ 'required' ],
			'password' => [ 'required' ],
			'password_repeat' => [ 'required', 'same:password']
		],
		'gig.create.venue-info' => [
	        'name' => 'required',
	        'address.first-line' => 'required|max:50',
	        'address.second-line' => 'max:50',
	        'address.county' => 'max:50',
	        'address.city' => 'required|max:30',
	        'address.postcode' => 'required',
	        'address.country' => 'max:60',
	        'url' => 'url',
	        'extra-details' => 'max:300',
	        'image' => 'file|max:1024',
			'image' => [ 'image', 'max:2048' ]
        ],
		'misc' => [
			'image' => [ 'image', 'max:2048' ]
		]
	];