<?php

	class LogItem extends BaseModel {

		public $table = 'log';

		public function object() {
			return $this->morphTo();
		}

		public function getData() {
			return json_decode($this->data);
		}

		public function getDescriptionAttribute() {
			switch($this->type) {
				case 'user.login':
					return '<span data-field="full_name">'.$this->object->full_name . '</span> logged in';
					break;

				case 'user.failed-login':
					return '<span data-field="full_name">'.$this->object->full_name . '</span> performed a failed login attempt';
					break;

				case 'user.logout':
					return '<span data-field="full_name">'.$this->object->full_name . '</span> logged out';
					break;

				case 'user.update-account':
					return '<span data-field="full_name">'.$this->object->full_name . '</span> updated their account';
					break;

				case 'user.update-password':
					return '<span data-field="full_name">'.$this->object->full_name . '</span> updated their password';
					break;

				case 'user.update-avatar':
					return '<span data-field="full_name">'.$this->object->full_name . '</span> updated their avatar';
					break;

				case 'user.update-profile':
					return '<span data-field="full_name">'.$this->object->full_name . '</span> updated their profile';
					break;

				case 'user.switch-account':
					$data = $this->getData();
					$u = User::find((int) $data->id);
					if(!$u) { $this->delete(); return false; }
					return '<span data-field="full_name">'.$this->object->full_name . '</span> switched account to <a href="' . url('admin/users/'.$u->id) . '">' . $u->full_name . '</a>';
					break;

				case 'user.switch-back':
					return '<span data-field="full_name">'.$this->object->full_name . '</span> switched back to their own account';
					break;

				case 'user.request-password-reset':
					return '<span data-field="full_name">'.$this->object->full_name . '</span> requested a password reset';
					break;

				case 'user.update-other-account':
					$data = $this->getData();
					$u = User::find((int) $data->id);
					if(!$u) { $this->delete(); return false; }
					return '<span data-field="full_name">'.$this->object->full_name . '</span> updated account <a href="' . url('admin/users/'.$u->id) . '">' . $u->full_name . '</a>';
					break;

				case 'user.reset-password': 
					return '<span data-field="full_name">'.$this->object->full_name . '</span> reset their password';
					break;

				case 'news.publish':
					$data = $this->getData();
					$p = News::withTrashed()->find((int) $data->id);
					if(!$p) { $this->delete(); return false; }
					return '<span data-field="full_name">'.$this->object->full_name .'</span> published news post <a href="' . route('admin.news.show', $p->id) . '">'.$p->title.'</a>';
					break;

				case 'news.unpublish':
					$data = $this->getData();
					$p = News::withTrashed()->find((int) $data->id);
					if(!$p) { $this->delete(); return false; }
					return '<span data-field="full_name">'.$this->object->full_name .'</span> unpublished news post <a href="' . route('admin.news.show', $p->id) . '">'.$p->title.'</a>';
					break;

				case 'news.post':
					$data = $this->getData();
					$p = News::withTrashed()->find((int) $data->id);
					if(!$p) { $this->delete();  return false; }
					return '<span data-field="full_name">'.$this->object->full_name .'</span> posted news post <a href="' . route('admin.news.show', $p->id) . '">'.$p->title.'</a>';
					break;

				case 'gig.create':
					$data = $this->getData();
					$gig = Gig::withTrashed()->find((int) $data->id);
					if(!$gig) { $this->delete(); return false; }
					return '<span data-field="full_name">'.$this->object->full_name .'</span> created gig <a href="' . route('admin.gigs.edit', $gig->id) . '">'.$gig->name.'</a>';
					break;

				default:
					return;
					break;

			}
		}
	}