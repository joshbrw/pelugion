<?php

	class BaseModel extends Eloquent {
		
		public function logs() {
			return $this->morphMany('LogItem', 'object');
		}
		
        public function photos() {
            return $this->morphToMany('Photo');
        }  
	}