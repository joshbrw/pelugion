<?php

    class PhotoAlbum extends BaseModel {

        public function object() {
            return $this->morphTo();
        }

        public function photos() {
            return $this->hasMany('Photo');
        }
    }