<?php

    class Photo extends BaseModel {

        /**
         * Laravel relationship for the photo album this photo belongs to
         * @return Relationship Many-to-one relationship of the Photo to the album
         */
        public function album() {
            return $this->belongsTo('PhotoAlbum');
        }

        /**
         * Query scope for locked photos (admin-editable only)
         * @param  Builder $query Query builder
         * @return Builder        Edited query builder
         */
        public function scopeLocked($query) {
            return $query->where('locked', 1);
        }

        /**
         * Query scope for unlocked photos (anyone can edit - default)
         * @param  Builder $query Query builder
         * @return Builder        Edited query builder
         */
        public function scopeUnlocked($query) {
            return $query->where('locked', 0);
        }
    }