<?php
    
    use Pelugion\Interfaces\AddressableInterface;
    use Pelugion\Interfaces\DisplayPhotoInterface;
    
    class Venue extends BaseModel implements AddressableInterface, DisplayPhotoInterface {

        public $table = 'venues';
        public $fillable = ['name', 'extra_details', 'addr_first_line', 'addr_second_line', 'addr_city', 'addr_county', 'addr_country', 'latitude', 'longitude', 'deleted_at', 'created_at', 'updated_at'];

        public function getAddressString($newlines = false) {
            $addressString = '';
            $keys = ['addr_first_line', 'addr_second_line', 'addr_city', 'addr_country', 'addr_county', 'addr_postcode'];
            foreach($keys as $i => $key) { 
                if($this->{$key}) {
                    if($key !== 'addr_first_line') {
                        $addressString .= ", "; 
                        if($newlines) $addressString .= "\n";
                    }

                    $addressString .= $this->{$key};

                    if(!isset($keys[$i + 1])) {
                        $addressString .= '.';
                    }
                }
            }
            return $addressString;
        }

        public function getAddressAttribute() {
            return $this->getAddressString(true);
        }

        public static function boot() {
            parent::boot();

            static::saving(function($address) {
                if($address->latitude == null || $address->longitude == null) {

                    $res = App::make('geocoder')->geocode($address->name . ', ' . $address->getAddressString());
                    $address->latitude = $res->getLatitude();
                    $address->longitude = $res->getLongitude();
                }
            }); 
        }     

        public function photoAlbums() {
            return $this->morphToMany('PhotoAlbum');
        }   

        public function gigs() {
            return $this->hasMay('Gig');
        }

        /**
         * Says if the object has a photo in the database
         * @return boolean Whether or not they have a photo
         */
        public function hasPhoto() {
            return $this->avatar_url != null;
        }

        /**
         * Get the URL of the photo
         * @return String URL of the photo (or placeholder)
         */
        public function getPhotoUrl() {
            if($this->avatar_url) {
                return asset(Config::get('app.image_display_path').$this->avatar_url);
            } else {
                return Config::get('app.placeholder_path');
            }
        }

        public function getPhotoPath() {
            if($this->avatar_url) {
                return $this->avatar_url;
            } else {
                return Config::get('app.placeholder_path');
            }
        }

        /**
         * Fetch the venues grouped by the city that they're in
         * @return array
         */
        public static function fetchGroupedByCity() {
            $venues = [];

            foreach(Venue::orderBy('addr_city', 'ASC')->get() as $venue) {
                $city = (isset($venue->addr_city) && $venue->addr_city != null) ? $venue->addr_city : 'Unknown City';

                $venues[$city][] = $venue;
            }

            return $venues;
        }
    }