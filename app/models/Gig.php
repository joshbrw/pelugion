<?php

    use Pelugion\Interfaces\DisplayPhotoInterface;
    use Pelugion\Interfaces\SluggableInterface;

    class Gig extends BaseModel implements DisplayPhotoInterface, SluggableInterface {

        public $table = 'gigs';
        public $dates = ['created_at','updated_at','deleted_at','date_time'];
        public $hidden = ['id'];
        public $softDelete = true;
        

        public function getSlug() {
            return Str::slug($this->name);
        }

        public function venue() {
            return $this->belongsTo('Venue');
        }

        /**
         * Says if the object has a photo in the database
         * @return boolean Whether or not they have a photo
         */
        public function hasPhoto() {
            return $this->image_url != null;
        }

        /**
         * Get the URL of the photo
         * @return String URL of the photo (or placeholder)
         */
        public function getPhotoUrl() {
            if($this->image_url) {
                return asset(Config::get('app.image_display_path') . "/" . $this->image_url);
            } else {
                /*
                 * If the gig doesn't have a photo return the venues photo function.
                 * This function will default to the placeholder image if the
                 * venue does not have an image.
                 */
                return $this->venue->getPhotoUrl();
            }
        }

        public function getPhotoPath() {
            if($this->image_url) {
                return asset($this->image_url);
            } else {
                /*
                 * If the gig doesn't have a photo return the venues photo function.
                 * This function will default to the placeholder image if the
                 * venue does not have an image.
                 */
                return $this->venue->getPhotoPath();
            }
        }

        /**
         * Get up and coming gigs ordered by their date and time ascending
         * @return Collection Colletion of gigs
         */
        public static function getUpcomingGigs() {
            return Gig::with('venue')->where('date_time', '>=', Time::now())->orderBy('date_time', 'ASC');
        }

        public function scopeUpcoming($query) {
            return $query->where('date_time', '>=', Time::now());
        }

        public function scopePast($query) {
            return $query->where('date_time', '<=', Time::now());
        }
    }