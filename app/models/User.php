<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use Pelugion\Interfaces\DisplayPhotoInterface;

class User extends BaseModel implements UserInterface, RemindableInterface, DisplayPhotoInterface {

	protected $table = 'users';
	public $fillable = ['first_name', 'last_name', 'email', 'password', 'date_of_birth', 'role', 'band_member', 'created_at', 'updated_at'];
	public $dates = ['created_at','updated_at','deleted_at','date_of_birth'];

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function isSuperAdmin() {
		return $this->role == 1;
	}

	public function instruments() {
		return $this->belongsToMany('Instrument', 'instrument_user');
	}

	public function getFullNameAttribute() {
		return $this->first_name . " " . $this->last_name;
	}


	public function resetToken() {
		return $this->hasOne('ResetToken');
	}

	public function alerts() {
		return $this->hasMany('Alert');
	}

	public function notices() {
		return $this->hasMany('Notice');
	}

	public function news() {
		return $this->hasMany('News', 'author_id');
	}

	public function hasAccessTo($object) {
		if($this->isSuperAdmin()) return true;

		switch(get_class($object)) {
			case "News":
				if($object->author_id == $this->id) return true;
				break;

			default:
				if($this->isSuperAdmin()) return true;
				return false;
		}
	}

	public function isBandMember() {
		if($this->band_member == 0) return false;
		return true;
	}

	public function getNameSlug() {
		return Str::slug($this->full_name);
	}

	public function getSlugAttribute() {
		return $this->getNameSlug();
	}

	public function scopeBandMembers($query) {
		return $query->where('band_member', '1');
	}

	public function getInstrumentsList() {
		if($this->instruments->count() == 0) return 'No Instruments';

		$list = '';

		$i = 1;
		foreach($this->instruments as $instrument) {
			$list.= $instrument->name;

			if($i != $this->instruments->count()) $list .= ' / ';

			$i++;
		}

		return $list;
	}

	public function generateAboutUrl() {
		if(!$this->isBandMember()) return false;

		return route("about.user", [ $this->id, Str::slug($this->full_name) ]);
	}

	public function photos() {
		return $this->morphToMany('Photo');
	}  

	/**
     * Says if the object has a photo in the database
     * @return boolean Whether or not they have a photo
     */
    public function hasPhoto() {
        return $this->avatar_url != null;
    }

    /**
     * Get the URL of the photo
     * @return String URL of the photo (or placeholder)
     */
    public function getPhotoUrl() {
        if($this->avatar_url) {
            return asset(Config::get('app.image_display_path').$this->avatar_url);
        } else {
            return Config::get('app.placeholder_path');
        }
    }

    public function getPhotoPath() {
    	return ($this->avatar_url) ? $this->avatar_url : Config::get('app.placeholder_path');
    }

}