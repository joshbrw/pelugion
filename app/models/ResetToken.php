<?php

	class ResetToken extends BaseModel {

		public $table = 'reset_tokens';
		public $softDelete = true;

		public static function generate(User $user) {
			// Delete all tokens that exist
			$tokens = ResetToken::where('user_id', '=', $user->id)->delete();

			$r = new ResetToken;
			$r->token = str_random(32);
			$r->user_id = $user->id;
			$r->expires = Time::now()->addHours(24)->toDateTimeString();
			$r->save();

			return $r;
		}

		public function user() {
			return $this->belongsTo('User', 'user_id');
		}

	}