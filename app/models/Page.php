<?php

    class Page extends BaseModel {

        public function contentSections() {
            return $this->hasMany('ContentSection');
        }
    }