<?php

	class News extends BaseModel {

		public $table = 'news';
		public $dates = [ 'published_at', 'created_at', 'updated_at', 'deleted_at' ];
		public $softDelete = true;
		public $with = [ 'author' ];

		public function author() {
			return $this->belongsTo('User', 'author_id');
		}

		public function scopePublished($query) {
			return $query->where('published_at', '<=', Time::now());
		}

		public function getSlug() {
			return Str::slug($this->title);
		}

		/**
		 * Return the News article's body
		 * @return string The articles body
		 */
		public function getBody() {
			return $this->body;
		}

		/**
		 * Get the articles subtext.
		 *
		 * If no subtext exists, the articles body is shortened and returned
		 * @return string Subtext or shortened body
		 */
		public function getSubtext() {
			if(!$this->subtext) {
				return shorten($this->body, 50);
			}

			return $this->subtext;
		}
	}