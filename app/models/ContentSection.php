<?php

    class ContentSection extends BaseModel {

        public $table = 'content_sections'; 

        public function page() {
            return $this->belongsTo('Page');
        }

        /**
         * Find section by . delimited string
         *
         * e.g. ContentSection::findSection('about.main')
         * 
         * @param  String $pageAndSection Dot-delimited string of the section's identifier
         * @return Boolean/ContentSection
         */
        public static function findSection($pageAndSection) {
            $arrayPageAndSection = explode('.', $pageAndSection);

            if(!count($arrayPageAndSection) == 2) return false;
            
            $pageIdentifierToSearch = $arrayPageAndSection[0];
            $sectionIdentifierToSearch = $arrayPageAndSection[1];

            try  {
                $page = Page::where('identifier', $pageIdentifierToSearch)->firstOrFail();
                $section = ContentSection::where('identifier', $sectionIdentifierToSearch)->where('page_id', $page->id)->firstOrFail();
            } catch(Exception $e) {
                return false;
            }

            return $section;
        }
    }