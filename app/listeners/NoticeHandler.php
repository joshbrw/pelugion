<?php

	class NoticeHandler {

		protected $user;

		public static function create($noticeMsg, $type, User $user) {
			$notice = new Notice;
			$notice->notice = trans('notices.' . $noticeMsg);
			$notice->type = $type;
			$notice->user_id = $user->id;
			$notice->save();

			return $notice;
		}

	}