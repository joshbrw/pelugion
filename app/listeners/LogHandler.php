<?php

	class LogHandler {

		public function login($user) {
			$log = new LogItem;
			$log->type = 'user.login';
			$log->ip = $_SERVER['REMOTE_ADDR'];
			$user->logs()->save($log);
		}

		public function failedLogin($user) {
			$log = new LogItem;
			$log->type = 'user.failed-login';
			$log->ip = $_SERVER['REMOTE_ADDR'];
			$user->logs()->save($log);
		}

		public function logout($user) {
			$log = new LogItem;
			$log->type = 'user.logout';
			$log->ip = $_SERVER['REMOTE_ADDR'];
			$user->logs()->save($log);
		}

		public function accountUpdate($user) {
			$log = new LogItem;
			$log->type = 'user.update-account';
			$log->ip = $_SERVER['REMOTE_ADDR'];
			$user->logs()->save($log);
		}

		public function passwordUpdate($user) {
			$log = new LogItem;
			$log->type = 'user.update-password';
			$log->ip = $_SERVER['REMOTE_ADDR'];
			$user->logs()->save($log);
		}

		public function avatarUpdate($user) {
			$log = new LogItem;
			$log->type = 'user.update-avatar';
			$log->ip = $_SERVER['REMOTE_ADDR'];
			$user->logs()->save($log);
		}

		public function profileUpdate($user) {
			$log = new LogItem;
			$log->type = 'user.update-profile';
			$log->ip = $_SERVER['REMOTE_ADDR'];
			$user->logs()->save($log);
		}

		public function switchAccount($oldUser, $newUser) {
			$log = new LogItem;
			$log->type = 'user.switch-account';
			$log->ip = $_SERVER['REMOTE_ADDR'];
			/**
			 * id is the ID of the user who updates the post
			 */
			$log->data = json_encode(array(
				'id' => $newUser->id
			));
			$oldUser->logs()->save($log);
		}

		public function switchBack($user) {
			$log = new LogItem;
			$log->type = 'user.switch-back';
			$log->ip = $_SERVER['REMOTE_ADDR'];
			$user->logs()->save($log);
		}

		public function requestPasswordReset($user) {
			$log = new LogItem;
			$log->type = 'user.request-password-reset';
			$log->ip = $_SERVER['REMOTE_ADDR'];
			$user->logs()->save($log);
		}

		public function updateOtherAccount($updater, $updated) {
			$log = new LogItem;
			$log->type = 'user.update-other-account';
			$log->ip = $_SERVER['REMOTE_ADDR'];
			$log->data = json_encode(array(
				'id' => $updated->id
			));

			$updater->logs()->save($log);
		}

		public function resetPassword($user) {
			$log = new LogItem;
			$log->type = 'user.reset-password';
			$log->ip = $_SERVER['REMOTE_ADDR'];
			$user->logs()->save($log);			
		}

		public function publishNews($post) {
			$log = new LogItem;
			$log->type = 'news.publish';
			$log->data = json_encode(array(
				'id' => $post->id
			));
			$log->ip = $_SERVER['REMOTE_ADDR'];
			Auth::user()->logs()->save($log);
		}

		public function unpublishNews($post) {
			$log = new LogItem;
			$log->type = 'news.unpublish';
			$log->data = json_encode(array(
				'id' => $post->id
			));
			$log->ip = $_SERVER['REMOTE_ADDR'];
			Auth::user()->logs()->save($log);
		}

		public function postNews($post) {
			$log = new LogItem;
			$log->type = 'news.post';
			$log->data = json_encode(array(
				'id' => $post->id
			));
			$log->ip = $_SERVER['REMOTE_ADDR'];
			Auth::user()->logs()->save($log);			
		}

		public function createGig($gig) {
			$log = new LogItem;
			$log->type = 'gig.create';
			$log->data = json_encode(array(
				'id' => $gig->id
			));
			$log->ip = $_SERVER['REMOTE_ADDR'];
			Auth::user()->logs()->save($log);	
		}
	}