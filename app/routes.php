<?php

Route::get('facebook', [ 'as' => 'facebook', function() { return Redirect::to('https://www.facebook.com/pages/Pelugion/1428073117412979'); }]);
Route::get('twitter', [ 'as' => 'twitter', function() { return Redirect::to('https://www.twitter.com/pelugion'); }]);

// Route::get('/', function() {return View::make('temp'); });

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::get('news/{id}/{slug?}', ['as' => 'news.show', 'uses' => 'NewsController@show']);

Route::get('about', ['as' => 'about.index', 'uses' => 'AboutController@index']);
Route::get('about/{id}/{slug}', [
	'uses' => 'AboutController@showUser',
	'as' => 'about.user'
]);

Route::get('contact', ['as' => 'contact.index', 'uses' => 'ContactController@index']);
Route::post('contact', ['as' => 'contact.send', 'uses' => 'ContactController@send']);

Route::get('gigs', [ 'as' => 'gigs.index', 'uses' => 'GigController@index' ]);
Route::get('gigs/{id}/{slug?}', [ 'as' => 'gigs.show', 'uses' => 'GigController@show']);

Route::get('music', [ 'as' => 'music.index', 'uses' => 'MusicController@index' ]);
Route::get('media', [ 'as' => 'media.index', 'uses' => 'MediaController@index' ]);


Route::group(array('prefix' => 'admin'), function() {

	Route::get('users/switch-back', ['before' => 'auth', 'uses' => 'AdminUserController@switchBack']);

	Route::group(array('before' => 'auth'), function() {
		Route::resource('users', 'AdminUserController');
		Route::post('users/{id}/updateSpecific', 'AdminUserController@updateSpecific');
		Route::get('users/switch/{id}', 'AdminUserController@switchUser');
		Route::get('log', 'AdminLogController@index');
		Route::get('news/delete-all', [ 'as' => 'admin.news.delete-all', 'uses' => 'AdminNewsController@deleteAll']);
	});
	
	Route::get('/', function() {
		if(!Auth::check()) {
			return Redirect::to('admin/login');
		} else {
			return Redirect::route('dash');
		}
	});


	Route::group(array('before' => 'auth'), function() {
		Route::get('dash', array('uses' => 'AdminDashController@index', 'as' => 'dash'));
		Route::post('account', ['before' => 'csrf', 'uses' => 'AdminAccountController@update']);
		Route::post('account/profile', ['before' => 'csrf', 'uses' => 'AdminAccountController@updateProfile']);
		Route::post('account/profile-image', ['before' => 'csrf', 'uses' => 'AdminAccountController@updateProfileImage']);
		Route::post('account/change-password', ['before' => 'csrf', 'uses' => 'AdminAccountController@updatePassword']);
		Route::get('account/remove-avatar', 'AdminAccountController@removeAvatar');
		Route::get('account', 'AdminAccountController@index');
		
		Route::get('logout', array('as' => 'logout', 'uses' => 'AdminLoginController@logout'));

		Route::resource('news', 'AdminNewsController');
		Route::get('news/{id}/publish', [
			'uses' => 'AdminNewsController@publish',
			'as' => 'admin.news.publish'
		]);
		Route::get('news/{id}/unpublish', [
			'uses' => 'AdminNewsController@unpublish',
			'as' => 'admin.news.unpublish'
		]);
		Route::get('news/{id}/delete', [
			'uses' => 'AdminNewsController@delete',
			'as' => 'admin.news.delete'
		]);

		Route::resource('content', 'AdminContentController');
		Route::post('content/update-section', 'AdminContentController@updateSection');

		Route::resource('gigs', 'AdminGigController');
		Route::post('gigs/{id}/update-image', [ 'as' => 'admin.gigs.update-image', 'before' => 'csrf', 'uses' => 'AdminGigController@updateImage']);

		Route::post('gigs/create/2', [
			'as' => 'admin.gigs.create.step-two',
			'uses' => 'AdminGigController@stepTwo'
		]);
		Route::get('gigs/create/2', 'AdminGigController@stepTwo');

		Route::post('gigs/create/3', [
			'as' => 'admin.gigs.create.step-three',
			'uses' => 'AdminGigController@stepThree'
		]);

		Route::get('gigs/create/3', 'AdminGigController@stepThree');

		Route::get('gigs/create/{wildcard}', [
			'as' => 'admin.gigs.reset-form',
			'uses' => 'AdminGigController@resetAndRedirect'
		]);

		Route::group([ 'prefix' => 'ajax' ], function() {
			Route::get('venues', 'AdminAjaxController@getAllVenues');
			Route::post('gigs/{id}/venue', 'AdminGigController@updateVenue');
		});
	});

	Route::group(array('before' => 'guest'), function() {
		Route::get('login/password-reset/{token}', 'AdminLoginController@resetPassword');
		Route::get('login/forgotten', 'AdminLoginController@displayForgotten');
		Route::post('login/forgotten', 'AdminLoginController@submitForgotten');
		Route::resource('login', 'AdminLoginController', array('only' => array('index', 'store')));
	});

});