@extends('layouts.master')

@section('main-content')
    <h1 class='clearfix section-header'>404 - Page Not Found</h1>
    Looks like that page can't be found. Why not use the navigation bar to find the page you were looking for?
@stop

@section('sidebar')
    <section class='sidebar-module sidebar-module--gigs clearfix'>
        <h1 class='section-header'>Useful Links</h1>
        <ul>
            <li><a href='#'>Homepage</a></li>
            <li><a href="#">Contact</a></li>
        </ul>
    </section>
@endsection