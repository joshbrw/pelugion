@extends('layouts.master')

@section('main-content')
    <h1 class='clearfix section-header'>{{ $gig->name }}</small></h1>

    <div class="single-gig">
        <div class="single-gig__image"><img src='{{ $gig->getPhotoUrl() }}' width='100%' /></div>
        <div class="single-gig__details">
            <div class="single-gig__description">{{ $gig->description }}</div>
            <div class="single-gig__date"><strong>Date</strong>: {{ $gig->date_time->format('D jS M Y @ h:iA') }}</div>
            <div class="single-gig__age-rating"><strong>Age Rating</strong>: {{ $gig->age_rating }}</div>
            <div class="single-gig__ticket-price"><strong>Ticket Price</strong>: 
                @if($gig->ticket_price == 0)
                    Free!
                @else
                    £{{ $gig->ticket_price }}
                @endif
            </div>
        </div>

        <div class='single-gig__venue'>
            <div class='venue__map' id="map"></div>
            <div class="venue__details">
                {{ $gig->venue->name }} - {{ nl2br($gig->venue->getAddressString()) }}<br />
                {{ nl2br($gig->venue->extra_details) }}
            </div>
        </div>
    </div>
    
@stop


@section('sidebar')
    @include('modules.upcoming-gigs')
@stop

@section('scripts')
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBkvHJW1gBkAkF7Mv9saBBuETKhdtg2dBk&sensor=false"></script>
    <script type="text/javascript">
      function initialize() {
        var venueLatLng = new google.maps.LatLng({{ $gig->venue->latitude }}, {{ $gig->venue->longitude}});

        var mapOptions = {
          center: venueLatLng,
          zoom: 16
        };
        var map = new google.maps.Map(document.getElementById("map"),
            mapOptions);

        var marker = new google.maps.Marker({
            position: venueLatLng,
            map: map,
            title: "{{ $gig->venue->name }}"
        });
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
@stop