@extends('layouts.master')

@section('main-content')
    <h1 class='clearfix section-header'>Upcoming Gigs</h1>
    
    <div id="gig-list">  
        @if(count($upcomingGigs) < 1)
            We currently have no Gigs coming up. Check back again soon!
        @else
            @foreach($upcomingGigs as $gig)
                <a href="{{ route('gigs.show', [ 'id' => $gig->id ]) }}">
                    <div class="gig-list__gig">  
                        <div class="gig-image">
                            <img src="{{ $gig->getPhotoUrl() }}" />
                        </div>
                        <div class='gig-details'>
                            <div class="gig-details__venue">
                                {{ $gig->name }} @ {{ $gig->venue->name }}
                            </div>
                            <div class="gig-details__info">
                                {{ $gig->date_time->format('D jS M Y') }}<br >
                                {{ $gig->date_time->format('H:ia') }}<br >
                                {{ $gig->venue->addr_city }}
                            </div>
                        </div>
                    </div>
                </a>
            @endforeach
        @endif
    </div>
@stop


@section('sidebar')
    @include('modules.random-ad')
@stop