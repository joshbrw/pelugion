@extends('layouts.master')

@section('main-content')
    <section class='cover-photo clearfix'>
        <img src='{{ asset("img/temp-cover-photo.png") }}' />
    </section>

    <h1 class='clearfix section-header'>{{ $post->title}}</h1>
    <article class='news-story'>
        <div class='news-story__meta'>
            Posted on {{ $post->published_at->format('l jS M Y') }} - by <a class='news-story__author' href='#'>{{ $post->author->full_name }}</a>
        </div>
        <div class='news-story__body'>
            {{ trim($markdownParser->transformMarkdown($post->getBody())) }}
        </div>
    </article>
@stop

@section('sidebar')
    @include('modules.upcoming-gigs')

    @include('modules.twitter-feed')
@stop