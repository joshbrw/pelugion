@extends('layouts.master')

@section('main-content')
    <h1 class="section-header">Music</h1>

    <h2>Pelugion - Believe</h2>
    <iframe id="ytplayer" type="text/html" class='band-video'
    src="http://www.youtube.com/embed/emN2HoQklE4?autoplay=0&origin=http://pelugion.dev"
    frameborder="0"/></iframe>

    <h2>Pelugion - Bury Me/Serpents Mistress</h2>
    <iframe id="ytplayer" type="text/html" class='band-video'
    src="http://www.youtube.com/embed/jLd0Dsbc-Go?autoplay=0&origin=http://pelugion.dev"
    frameborder="0"/></iframe>

    <h2>Pelugion - Remedy</h2>
    <iframe id="ytplayer" type="text/html" class='band-video'
    src="http://www.youtube.com/embed/MRzO5GOWdeI?autoplay=0&origin=http://pelugion.dev"
    frameborder="0"/></iframe>

@stop

@section('sidebar')
    @include('modules.random-ad')
@stop