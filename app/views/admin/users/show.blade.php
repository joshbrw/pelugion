@extends('layouts.admin')

@section('content')	
	<a href='{{ URL::to('admin/users') }}' class='button small'><i class='icon-chevron-sign-left'></i> Go Back</a>
	<h1 class='page-title'><span data-field="full_name">{{$user->full_name}}</span></h1>
	<div class='panel block-grid small-12 edit-tools align-center'>
		<p><a class='button tiny small-12 medium-3
		@if($user->id == Auth::user()->id)
			disabled' disabled href='#'
		@else
			' href='{{ url("admin/users/switch/".$user->id) }}'
		@endif
		 >Switch to User</a>

		 <a href="{{ url('admin/users/'.$user->id.'/edit') }}" data-hint='Edit User' class="button tiny small-12 medium-3 hint--right hint--rounded">Edit User</a>
	</div>

	<div class="small-12 large-4 columns">
		<div class="caption align-center">
				<h3 class='small-12'>User Information </h3>
						
				
				<p>
					<table class='small-12'>
					<tr>
						<th>Name:</th><td id='full_name' data-id='{{ $user->id }}' class='edit'>{{ $user->full_name }}</td>
					</tr>
					<tr>
						<th>Nickname:</th><td id='nickname' data-id='{{ $user->id }}' class='edit'>{{ $user->nickname }}</td>
					</tr>
					<tr>
						<th>Email:</th><td id='email' data-id='{{ $user->id }}' class='edit'>{{ $user->email }}</td>
					</tr>
					<tr>
						<th>Date of Birth:</th><td id='date_of_birth' data-id='{{ $user->id }}' class='edit'>{{ Time::parse($user->date_of_birth)->day }}-{{ Time::parse($user->date_of_birth)->month }}-{{ Time::parse($user->date_of_birth)->year }}</td>
					</tr>
					<tr>
						<th>Role:</th><td>@if($user->isSuperAdmin()) Super Admin @else Regular Admin @endif</td>
					</tr>
					<tr>
						<th>Band Member:</th><td>@if($user->band_member)
													<a href='#' class='btn btn-success btn-xs'><i class='icon-ok-sign'></i></a>
												@else
													<a href='#' class='btn btn-danger btn-xs'><i class='icon-remove-sign'></i></a>
												@endif</td>
					</tr>
					<tr>
						<th>Last Login:</th><td>
											@if(!$last_login)
												N/A
											@else
												{{ Time::parse($last_login->created_at)->diffForHumans() }}
											@endif
											</td>
					</tr>
					</table>
				</p>
				<p></p>
			</div>
	</div>

	<div class="small-12 large-8 columns" id="log">
			<div class="caption">
				<h3>User Log</h3>
				<p>
					<table class='small-12'>
						<tr>
							<td colspan='3'>
								<div class='small-6 large-3 columns'>
									<a href='#' data-dropdown='filters' class='button dropdown tiny small-12 columns'>Filters</a><br />

									<ul id="filters" data-dropdown-content class='f-dropdown'>
										<li><a href="{{ URL::current() }}?filter=login#log">Logins</a></li>
										<li><a href="{{ URL::current() }}?filter=logout#log">Logouts</a></li>
										<li><a href="{{ URL::current() }}?filter=failed-login#log">Failed Logins</a></li>
										<li><a href="{{ URL::current() }}?filter=update-account#log">Account Updates</a></li>
										<li><a href="{{ URL::current() }}?filter=request-password-reset#log">Password Reset Requests</a></li>
										<li><a href="{{ URL::current() }}?filter=post-news#log">Posted News Story</a></li>
										<li><a href="{{ URL::current() }}?filter=publish-news#log">Published News Story</a></li>
										<li><a href="{{ URL::current() }}?filter=unpublish-news#log">Unpublished News Story</a></li>
									</ul>
								</div>
								
								<div class='small-6 large-3 align-right columns'>
									<a type="button" href="{{ URL::current() }}#log" class="button tiny alert small-12 columns">Reset Filters</a>
								</div>
							</td>
						</tr>
						<tr>
							<th>Action</th>
							<th>IP Address</th>
							<th>Time</th>
						</tr>
						@if($log_items->count() == 0)
						<tr>
							<td colspan='3'>No items</td>
						</tr>
						@else
							@foreach($log_items as $item)
								@if($item->description != false) 
									<tr>
										<td>{{ $item->description }}</td>
										<td>{{ $item->ip }}</td>
										<td>{{ Time::parse($item->created_at)->diffForHumans() }}</td>
									</tr>
								@endif
							@endforeach
						@endif
					</table>	
					{{ $log_items->links() }}
				</p>
			</div>
	</div>
@stop