@extends('layouts.admin')

@section('content')

	<h1 class='page-title'>Manage Users</h1>

	<table class="small-12">
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Created</th>
				<th>Updated</th>
			</tr>
		</thead>
		<tbody>
			@foreach($users as $user)
				<tr class='clickable' href='{{ url("admin/users/".$user->id) }}'>
					<td>{{ $user->full_name }}</td>
					<td>{{ $user->email }}</td>
					<td>{{ Time::createFromTimestamp(strtotime($user->created_at))->diffForHumans() }}</td>
					<td>
						@if($user->updated_at != $user->created_at)
							{{ Time::createFromTimestamp(strtotime($user->updated_at))->diffForHumans() }}
						@else
							N/A
						@endif
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>

@stop