@extends('layouts.admin')

@section('content')

	<h1 class='page-title'>Edit User: {{ $user->full_name }}</h1>
	
	<div class='panel'>
	{{ Form::open(array('url' => 'admin/users/'.$user->id, 'method' => 'PUT', 'class' => 'form-horizontal'))}}

		<div class='small-12'>
		<label for='first_name'>First Name <span class='redtext'>*</span></label>
		<input type='text' class='form-control' name='first_name' placeholder='First Name' value='{{ $user->first_name }}' />
		</div>

		<div class='small-12'>
		<label for='last_name'>Last Name <span class='redtext'>*</span></label>
		<input type='text' class='form-control' name='last_name' placeholder='Last Name' value='{{ $user->last_name }}' />
		</div>

		<div class='small-12'>
		<label for='email'>Email Address <span class='redtext'>*</span></label>
		<input type='email' class='form-control' name='email' placeholder='Email' value='{{ $user->email }}' />
		</div>

		<div class='small-12'>
		<label for='email'>Is this user a band member? </label>
		<input type='checkbox' class='form-control' name='band_member' 
			@if($user->band_member == 1)
				checked
			@endif
		/>
		</div>

		<div class='small-12'>
		<label for='instruments'>Which instruments do you play? <span class='redtext'>*</span></label>
		@foreach($instruments as $instrument)
			<input type='checkbox' name='instruments[{{ $instrument->id }}]'
			@if(in_array($instrument->id, $instruments_list))
				checked
			@endif
			/> {{ $instrument->name }}<br />
		@endforeach
		</div>

		<div class='small-12'>
		<label for='role'>Role <span class='redtext'>*</span></label>
		<select class='form-control' name='role'>
			<option value='0' @if(!$user->isSuperAdmin()) selected @endif>Admininstrator</option>
			<option valud='1' @if($user->isSuperAdmin()) selected @endif>Super Administrator</option>
		</select>
		</div>

		<div class='small-12'>
		<label for='password'>New Password</label>
		<input type='password' class='form-control' name='password'  />
		</div>

		<div class='small-12'>
		<label for='password_confirm'>New Password (repeat)</label>
		<input type='password' class='form-control' name='password_confirm'  />
		</div>

		<button type="submit" class="btn btn-default"><i class="icon-check-sign"></i> Update Account</button>

		</div>
	</form>

@endsection