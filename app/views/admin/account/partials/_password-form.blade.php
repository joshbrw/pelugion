<div class="panel panel-default change-password">
    <div class="panel-heading">
        <h3 class="panel-title">Change Your Password</h3>
    </div>
    <div class="panel-body">
        {{ Form::open(array('url' => 'admin/account/change-password', 'method' => 'POST', 'class' => 'form-horizontal'))}}

            <div class="large-12">
                <label for='current_password' class='{{ $errors->has('current_password') ? 'error' : '' }} '>Current Password <span class='redtext'>*</span></label>

                <input type='password' name='current_password' placeholder='Current Password' required {{ $errors->first('current_password', "class='error'") }} />
                
                {{ $errors->first('current_password', '<small class="error">:message</small>') }}
            </div>

            <div class="large-12">
                <label for='password' class='{{ $errors->has('password') ? 'error' : '' }} '>New Password <span class='redtext'>*</span></label>

                <input type='password' name='password' required placeholder='New Password' {{ $errors->first('password', "class='error'") }} />
                
                {{ $errors->first('password', '<small class="error">:message</small>') }}
            </div>

            <div class="large-12">
                <label for='password_repeat' class='{{ $errors->has('password_repeat') ? 'error' : '' }} '>New Password (Repeated) <span class='redtext'>*</span></label>

                <input type='password' name='password_repeat' required placeholder='New Password (Repeat)' {{ $errors->first('password_repeat', "class='error'") }} />
                
                {{ $errors->first('password_repeat', '<small class="error">:message</small>') }}
            </div>

            <div class="form-group">
                <div class="align-right small-12">
                    <button type="submit" class="button small success small-12"><i class="icon-check-sign"></i> Change Password</button>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>