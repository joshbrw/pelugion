<div class="panel panel-default change-details">
    <div class="panel-heading">
        <h3 class="panel-title">Your Profile</h3>
    </div>
    <div class="panel-body">
        {{ Form::open(array('url' => 'admin/account/profile', 'method' => 'POST', 'class' => 'form'))}}
            <div>
                <label><h4>Bio</h4></label>
                <div class='col-lg-12'>
                    <textarea rows='8' class='form-control tinymce' name='bio'>{{ $user->bio }}</textarea>
                    {{ $errors->first('bio', '<span class="label label-danger">:message</span>') }}
                </div> 
            </div>

            <br />
            <div>
                <label><h4>Interests</h4></label>
                <div class='col-lg-12'>
                    <textarea rows='8' class='form-control tinymce' name='interests'>{{ $user->interests }}</textarea>
                    {{ $errors->first('interests', '<span class="label label-danger">:message</span>') }}
                </div> 
            </div>

            <br />
            <div>
                <label><h4>Influences</h4></label>
                <div class='col-lg-12'>
                    <textarea rows='8' class='form-control tinymce' name='influences'>{{ $user->influences }}</textarea>
                    {{ $errors->first('influences', '<span class="label label-danger">:message</span>') }}
                </div> 
            </div>

            <br />
            <div class="form-group">
                <div class="align-right small-12">
                    <button type="submit" class="button small success small-12"><i class="icon-check-sign"></i> Update Profile</button>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>
