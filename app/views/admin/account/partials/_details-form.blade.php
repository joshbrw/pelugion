<div class="panel panel-default change-details">
    <div class="panel-heading">
        <h3 class="panel-title">Your Details</h3>
        {{ Form::open(array('url' => 'admin/account', 'method' => 'POST', 'class' => 'form-horizontal'))}}

            <div class="small-12 {{ $errors->first('first_name', 'error') }}">
                <label for='first_name'>First Name <small>required</small></label>

                <input type='text' name='first_name' required placeholder='John' value='{{ $user->first_name }}' {{ $errors->first('first_name', "class='error'") }} />

                {{ $errors->first('first_name', '<small class="error">:message</small>') }}
            </div>

            <div class="small-12 {{ $errors->first('last_name', 'error') }}">
                <label for='last_name'>Surname <small>required</small></label>

                <input type='text' name='last_name' required placeholder='Doe' value='{{ $user->last_name }}' {{ $errors->first('last_name', "class='error'") }} />

                {{ $errors->first('last_name', '<small class="error">:message</small>') }}
            </div>

            <div class="small-12 {{ $errors->first('nickname', 'error') }}">
                <label for='nickname'>Nickname</label>

                <input type='text' name='nickname' placeholder='Nickname' value='{{ $user->nickname }}' {{ $errors->first('nickname', "class='error'") }} />

                {{ $errors->first('nickname', '<small class="error">:message</small>') }}
            </div>


            <div class='small-12'>
                <label for='date_of_birth'>Date of Birth:</label>

                <div class='row'>
                    <div class='large-4 columns'>
                       <select name='date'>
                            @foreach(range(1,31) as $num) 
                                <option value="{{ $num }}"
                                    @if($num == $user->date_of_birth->format('d'))
                                        selected
                                    @endif 
                                >{{ $num }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class='large-4 columns'>
                        <select name='month'>
                            @foreach($months as $month) 
                                <option value="{{ $month }}"
                                @if($month == $user->date_of_birth->format('F'))
                                    selected
                                @endif 
                            >{{ $month }}</option>
                            @endforeach
                        </select>
                    </div> 

                    <div class='large-4 columns'>
                        <select name='year'>
                            @foreach(range(1900,date('Y')) as $year) 
                                <option value="{{ $year }}"
                                    @if($year == $user->date_of_birth->format('Y'))
                                        selected
                                    @endif 
                                >{{ $year }}</option>
                            @endforeach
                        </select>   
                    </div>

                    {{ $errors->first('date_of_birth', '<small class="error">:message</small>') }}
                </div>
            </div>

            <div class="small-12">
                <label for='email' class='{{ $errors->has('email') ? 'error' : '' }} '>Email Address <span class='redtext'>*</span></label>

                <input type='email' name='email' required placeholder='email' value='{{ $user->email }}' {{ $errors->first('email', "class='error'") }} />
                
                {{ $errors->first('email', '<small class="error">:message</small>') }}
            </div>


        
            
            <div class='instruments-selector'>
                <label for='instruments' class='col-lg-4 control-label'>My Instruments</label>
                <div class='small-12'>
                    @foreach($instruments as $instrument)
                        <input type='checkbox' name='instruments[{{ $instrument->id }}]'
                        @if(in_array($instrument->id, $instruments_list))
                            checked
                        @endif
                        /> {{ $instrument->name }}
                    @endforeach
                    {{ $errors->first('instuments', '<small class="error">:message:</small>') }}
                </div>
            </div>
            <div>
                <button type="submit" class="button small success small-12"><i class="icon-check-sign"></i> Update Account</button>
            </div>
        {{ Form::close() }}
    </div>
</div>