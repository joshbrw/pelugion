<div class="panel panel-default change-avatar">
    <div class="panel-heading">
        <h3 class="panel-title">Update Your Profile Image</h3>
    </div>
    <div class="panel-body">
        {{ Form::open(array('files' => true, 'url' => 'admin/account/profile-image',  'method' => 'POST', 'class' => 'form-horizontal')) }}
        <div class='large-3 columns'>
            <img width='200' height='200' src='{{ Auth::user()->getPhotoUrl() }}' />
        </div>
        <div class='large-9 push-1 columns'>
            {{ Form::file('profile_image') }}
            {{ $errors->first('profile_image', '<span class="error">:message</span>') }}
            <p class="help-block">Max 2MB PNG/JPEG. Images are automatically adjusted to 100x100 pixels.</p>
        </div>
        <a href="{{ url('admin/account/remove-avatar') }}" class="button small alert small-6 columns"><i class="icon-remove-sign"></i> Remove Avatar</a>
        <button type="submit" class="button small success small-6"><i class="icon-check-sign"></i> Update Avatar</button>
        {{ Form::close() }}
    </div>
</div>