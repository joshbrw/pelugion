@extends('layouts.admin')

{{-- Stop that damn errors section showing. Using specific errors per-section on this page. --}}

@section('content')
	<h1 class='page-title'>Account, Profile &amp; Password</h1>

	<div class='row'>
		<div class='small-12 large-6 columns'>
			@include('admin.account.partials._details-form')

			@include('admin.account.partials._password-form')
		</div>
		<div class='small-12 large-6 columns'>
			@include('admin.account.partials._avatar-form')

			@include('admin.account.partials._profile-form')
		</div>
	</div>
@stop