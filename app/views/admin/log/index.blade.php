@extends('layouts.admin')

@section('content')
	<h1 class='page-title'>Activity Log</h1>

	<table class="small-12">
		<thead>
			<tr>
				<th>Type</th>
				<th>User</th>
				<th>IP Address</th>
				<th>Time</th>
			</tr>
		</thead>
		<tbody>
			@if($log_items->count() == 0)
			<tr>
				<td colspan='3'>No log items found.</td>
			</tr>
			@endif 

			@foreach($log_items as $item)	
				@if($item->description != false)
					<tr>
						<td>{{ $item->description }}</td>
						<td><a href='{{ url("admin/users/".$item->object->id) }}'>{{ $item->object->full_name }}</a></td>
						<td>{{ $item->ip }}</td>
						<td>{{ Time::createFromTimestamp(strtotime($item->created_at))->diffForHumans() }}</td>
					</tr>
				@endif
			@endforeach
		</tbody>
	</table>
	{{ $log_items->links() }}
@stop
