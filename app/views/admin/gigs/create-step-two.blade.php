@extends('layouts.admin')

@section('content')
    
    {{ Form::open([
        'route' => 'admin.gigs.create.step-three',
        'class' => 'form-horizontal',
        'method' => 'POST',
        'files' => true,
        'id' => 'create-gig-form'
    ]) }}
    <h1>Create a new Gig <span class='small'>at {{ $venue->name }}</span></h1>
    <h3>Step 2: The Gig</h3>
    <p>In this step you need to provide information about the gig itself.</p>

    <div class='panel small-12'>
    <label for="gig[name]">Name <span class='redtext'>*</span></label>
    {{ Form::text('gig[name]', Input::old('gig[name]'), [
        'class' => 'form-control',
        'placeholder' => 'Gig Name',
        'required'
    ]) }}

    <label for="gig[date-time]">Date & Time <span class='redtext'>*</span></label>
    <input type='datetime-local' class='form-control' name='gig[date-time]' required />

    <label for="gig[description]">Description <span class="redtext">*</span></label>
    {{ Form::textarea("gig[description]", Input::old("gig[description]"), [
        "class" => "form-control",
        "placeholder" => "e.g. Who are you supporting, are any bands supporting you?",
        'required'
    ]) }}

    
    <label for="gig[ticket-price]">Ticket Price <span class='redtext'>*</span>
    (If free, enter 0)</label>

    <div class="row collapse">
        <div class="small-3 large-2 columns">
            <span class="prefix">£</span>
        </div>
        <div class="small-9 large-10 columns">
            {{ Form::text('gig[ticket-price]', Input::old('gig[ticket-price]'), [
                'placeholder' => '10',
                'required'
            ]) }}
        </div>
    </div>
    

    <label for="gig[age-rating]">Age Rating <span class="redtext">*</span></label>
    <select name='gig[age-rating]' class='form-control' id='age-select'>
        <option value='All Ages Allowed'>All Ages Allowed</option>
        <option value='14+'>14+</option>
        <option value='16+'>16+</option>
        <option value='18+'>18+</option>
        <option value='Other'>Other</option>
    </select>

    <div id='select-other-age' class='hidden margin-t-15'>
        <input type='text' name='gig[other-age-rating]' class='form-control' placeholder="e.g. Under 16's allowed with adult" />
    </div>

    <label for="gig[image]">Gig Image <br />
    (Displayed on homepage, Venue image used if not selected)</label>
    <input type='file' name='gig[image]' />

    </div>
    <div class='row'>
        <div class='small-6 columns'>
            <a href='{{ route("admin.gigs.reset-form") }}' class='button small alert'>Reset</a>
        </div>
        <div class='small-6 columns align-right'>
            <input type='submit' class='button small ' value='Next Step - Finish' />
        </div>
    </div>
@stop


@section('scripts')
    @parent
        
    <script type="text/javascript">
        $(function() {
            $('#age-select').change(function(event) {
                if($("#age-select :selected").val() == "Other") {
                    // $(this).attr('disabled', function(idx, oldAttr) {
                    //     return !oldAttr;
                    // });
                    
                    $('#select-other-age').removeClass('hidden');
                } else {
                    $('#select-other-age').addClass('hidden');
                }
            });

        });
    </script>
@stop