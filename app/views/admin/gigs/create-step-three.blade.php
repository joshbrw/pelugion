@extends('layouts.admin')

@section('content')

    <h1>Gig {{ $gig->name }} Created!</h1>

    <div class='panel'>
        <div class='small-12'>
            Your gig has been created! Details are as follows:<br /><br />

            <strong>Name:</strong> {{ $gig->name }}<br />
            <strong>Description:</strong> {{ $gig->description }}<br />
            <strong>Date/Time:</strong> {{ $gig->date_time }}<br />
            <strong>Age Rating:</strong> {{ $gig->age_rating }}<br />
            <strong>Ticket Price:</strong> £{{ $gig->ticket_price }}<br />
        </div>
    </div>
@stop