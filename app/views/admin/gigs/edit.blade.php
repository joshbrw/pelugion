@extends('layouts.admin')

@section('content')
    <h1 class='page-title'>Edit Gig: {{ $gig->name }}</h1>

    @include('admin.gigs._partials.edit-gig-details')
    @include('admin.gigs._partials.change-venue')
    @include('admin.gigs._partials.change-avatar')
@stop