@extends('layouts.admin')

@section('content')

    {{ Form::open([
        'route' => 'admin.gigs.create.step-two',
        'class' => 'form-horizontal',
        'method' => 'POST',
        'files' => true,
        'id' => 'create-gig-form'
    ]) }}
    <h1>Create a new Gig</h1>
    <h3>Step 1: The Venue</h3>
    <p>In this step you need to choose the venue where your gig is happening. If you are playing at a venue you have previously created use the button on the left otherwise create a new venue on the right.</p>

    <div class='panel'>
    <div class='small-12 align-center' id='venue-decider'>
        {{-- Deciding either to use an old venue or create one --}}
        <div class='small-12' id='choose-or-new'>
            <button type='button' class='btn btn-primary btn-lg choose-or-new-btn' data-action='choose'>Choose an existing venue</button> or <button type='button' class='btn btn-primary btn-lg choose-or-new-btn' data-action='new'>Create a new venue</button>
        </div>
    
        {{-- Choosing an existing venue --}}
        <div class='small-6 align-left' id='choose-existing-venue'>
            <h4>Choose an existing venue:</h4>
            <strong>Search for an existing venue:</strong><br /><br />
            <input class='small-12' type='text' data-id='0' size='100' id='choose-existing-venue-input' />
        </div>

        {{-- Creating venue --}}
        <div class='small-6 align-left' id='create-new-venue'>
            @include('admin.gigs._partials.create-venue-form')
        </div>
    </div>

    @include('admin.gigs._partials.templates')

    <div class='selected-venue'>
    </div>
    </div>

    <div class='row'>
        <div class='small-6 columns'>
            <a href='{{ route("admin.gigs.reset-form") }}' class='button small alert'>Reset</a>
        </div>
        <div class='small-6 columns align-right'>
            <input type='submit' class='button small' value='Next Step - The Gig' disabled />
        </div>
    </div>
@stop

@section('scripts')
    @parent

    <script type="text/javascript" src="{{ asset('admin-res/js/mustache.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin-res/js/Hogan.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin-res/js/typeahead.min.js') }}"></script>

    <script type="text/javascript">
        $(function() {
            $('#choose-existing-venue').hide();
            $('#create-new-venue').hide();

            $('#choose-existing-venue-input').typeahead({                                
              name: 'venues',                                                          
              local: {{ json_encode($venues) }},                                        
              limit: 10,
              template: '@{{value}} <span class="small light">@{{address}}</span> ',
              engine: Hogan                                                         
            })

            .on('typeahead:selected', function (object, datum) {
                var venue = datum;

                $('input[name=chosen]').remove();
                $('input[name=choose-or-new]').after('<input type="hidden" name="chosen" value="' + datum.id + '" />');
                $("#choose-existing-venue-input").val(datum.value);
                $('input[type=submit]').removeAttr('disabled');

                $('#venue-decider').hide();
                $('.selected-venue').html(Mustache.render($('#tpl-selected-venue').html(), datum)).show();
            });

            $('.choose-or-new-btn').click(function(e) {
                $('#choose-or-new').hide();
                if($(this).attr('data-action') == 'choose') {
                    $('input').each(function(e) {
                        $(this).removeAttr('required');
                    });
                    $('input[name=_token]').after('<input type="hidden" name="choose-or-new" value="choose" />');
                    $('#choose-existing-venue').show();
                } else {
                    $('input[name=_token]').after('<input type="hidden" name="choose-or-new" value="new" />');
                    $('#create-new-venue').show();
                    $('input[type=submit]').removeAttr('disabled');
                }
            });
        }); 
    </script>
@stop