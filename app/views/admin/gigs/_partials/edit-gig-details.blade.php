<div class="small-12 large-6 columns">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Edit Gig Information</h3>
        </div>
        <div class="panel-body">   
            {{ Form::open([ 
                'route' => ['admin.gigs.update', $gig->id],
                'method' => 'PATCH'
            ])}}
            <div class="small-12 {{ $errors->first('name', 'error') }}">
                <label for='name'>Gig Name <small>required</small></label>

                <input type='text' name='name' required placeholder='Gig Name' value='{{ $gig->name }}' {{ $errors->first('name', "class='error'") }} />

                {{ $errors->first('name', '<small class="error">:message</small>') }}
            </div>
            <div class="small-12 {{ $errors->first('description', 'error') }}">
                <label for='name'>Description <small>required</small></label>
                
                <textarea name="description" cols="30" rows="15" required placeholder="Description" {{ $errors->first('description', "class='error'") }}>{{ $gig->description }}</textarea>

                {{ $errors->first('name', '<small class="error">:message</small>') }}
            </div>
            <div class="small-12 {{ $errors->first('date-time', 'error') }}">
                <label for='name'>Gig Date <small>required</small></label>
                
                <input type='datetime-local' class='form-control' name='date-time' required value="{{ \Pelugion\Date\Formatter::forDateTimeLocal($gig->date_time)}}" {{ $errors->first('date-time', "class='error'") }} />

                {{ $errors->first('date-time', '<small class="error">:message</small>') }}
            </div>
            <div class="small-12 {{ $errors->first('ticket-price', 'error') }}">
                <label foroutesr="ticket-price">Ticket Price (If free, enter 0) <small>required</small></label>

                <div class="row collapse">
                    <div class="small-3 large-2 columns">
                        <span class="prefix">£</span>
                    </div>
                    <div class="small-9 large-10 columns">
                        {{ Form::text('ticket-price', $gig->ticket_price, [
                            'placeholder' => '10',
                            'required',
                        ]) }}
                    </div>
                </div>

                {{ $errors->first('ticket-price', '<small class="error">:message</small>') }}
            </div>
            <div class="small-12 {{ $errors->first('age-rating', 'error') }}">                
                <label for="age-rating">Age Rating <small>required</small></label>
                <select name='age-rating' class='form-control' id='age-select'>
                    <option value='All Ages Allowed'>All Ages Allowed</option>
                    <option value='14+'>14+</option>
                    <option value='16+'>16+</option>
                    <option value='18+'>18+</option>
                    <option value='Other'>Other</option>
                </select>

                <div id='select-other-age' class='hidden margin-t-15'>
                    <input type='text' name='other-age-rating' class='form-control' placeholder="e.g. Under 16's allowed with adult" />
                </div>

                {{ $errors->first('age', '<small class="error">:message</small>') }}
            </div>
            <div class="small-12">
                <input type="submit" class="button tiny success" />
            </div>
			{{ Form::close() }}
        </div>
    </div>
</div>

@section('scripts')
    @parent
        
    <script type="text/javascript">
        $(function() {
            $('#age-select').change(function(event) {
                if($("#age-select :selected").val() == "Other") {
                    // $(this).attr('disabled', function(idx, oldAttr) {
                    //     return !oldAttr;
                    // });
                    
                    $('#select-other-age').removeClass('hidden');
                } else {
                    $('#select-other-age').addClass('hidden');
                }
            });

        });
    </script>
@stop