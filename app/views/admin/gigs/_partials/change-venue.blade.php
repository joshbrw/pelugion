<div class="small-12 large-6 columns">
    <div class="panel-default panel">
        <div class="panel-heading">
            <h3 class="panel-title">Change Venue</h3>
        </div>
        <div class="panel-body">
            <div data-alert class="alert-box radius" id="venueMessage" style="display: none">
                <span class="message"></span>
                <a href="#" class="close">&times;</a>
            </div>
            
            <select name='venue' id='venue-changer'>
                @foreach($venues as $city => $venues)
                    <optgroup label="{{ $city }}">
                    @foreach($venues as $venue) 
                        <option value="{{ $venue['id'] }}" 
                            @if($venue['selected'])
                                selected
                            @endif
                        >{{ $venue['name'] }}</option>
                    @endforeach
                    </optgroup>
                @endforeach
            </select>
        </div>
    </div>
</div>


<script>
        var venueSelect = document.getElementById('venue-changer');
        var urlToUpdateVenue = '{{ url("admin/ajax/gigs/" . $gig->id . "/venue") }}';
        var venueMessageDiv = document.getElementById('venueMessage');
        var venueMessage = venueMessageDiv.querySelector('span.message');

        venueSelect.addEventListener('change', venueSelectorChanged);

        /**
         * Update the gigs venue via AJAX
         */
        function venueSelectorChanged(e) {
            var xhr = new XMLHttpRequest();
            var formData = new FormData();

            formData.append('venueId', e.target.selectedOptions[0].value);
            xhr.open("POST", urlToUpdateVenue);

            xhr.onreadystatechange = function() {
                if(xhr.readyState == 4) {
                    if(xhr.status == 200) {
                        console.log('before success');
                        venueSuccess();
                    } else {
                        console.log('before error');
                        venueError();
                    }
                }
            };

            xhr.send(formData);
        }

        /**
         * Show an error when updating the venue
         */
        function venueError() {
            console.log('error');
            venueMessage.innerHTML ="An error occured.. Please reload the page and try again.";
            venueMessageDiv.classList.add('error');
            venueMessageDiv.style.display = "block";
        }

        /**
         * Show success when updating the venue
         */
        function venueSuccess() {
            console.log('success');
            venueMessage.innerHTML ="Successfully updated venue!";
            venueMessageDiv.classList.add('success');
            venueMessageDiv.style.display = "block";

            setTimeout(function() {
                console.log('timed out success');
                venueMessageDiv.style.display = "none";
            }, 3000);
        }
    </script>