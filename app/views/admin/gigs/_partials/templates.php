<script type='text/html' id="tpl-selected-venue">
    <div class='row'>
        <div class='col-xs-2'>
            <img src="{{image}}" height='100px'/>
        </div>
        <div class='col-xs-10'>
            You have selected venue {{label}}
        </div>
    </div>
</script>