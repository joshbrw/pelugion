<h4>Create a new venue:</h4> 
<p>Please create a new venue using the form below. Please fill in the form as accurately as you can as this information will be displayed on the website.<br /> All fields marked with <span class='redtext'>*</span> are <stong>required</stong>.</p>
<div class='form-group'>
    <label class="col-xs-4" for="new-venue[name]">Venue Name <span class='redtext'>*</span></label>
    <div class='col-xs-8'>
        {{ Form::text('new-venue[name]', Input::old('new-venue[name]'), [
            'class' => 'form-control',
            'placeholder' => 'Kasbah Coventry'
        ]) }}
    </div>
</div>  

<div class='form-group'>
    <label class="col-xs-4" for="new-venue[address]">Address <span class='redtext'>*</span></label>
    <div class='col-xs-8'>
        {{ Form::text('new-venue[address][first-line]', Input::old('new-venue[address][first-line]'), [
            'class' => 'form-control margin-b-15',
            'placeholder' => 'First Line',
            'required'
        ]) }}
        {{ Form::text('new-venue[address][second-line]', Input::old('new-venue[address][second-line]'), [
            'class' => 'form-control margin-b-15',
            'placeholder' => 'Second Line (optional)'
        ]) }}
        {{ Form::text('new-venue[address][city]', Input::old('new-venue[address][city]'), [
            'class' => 'form-control margin-b-15',
            'placeholder' => 'City',
            'required'  
        ]) }}
        {{ Form::text('new-venue[address][county]', Input::old('new-venue[address][county]'), [
            'class' => 'form-control margin-b-15',
            'placeholder' => 'County (optional)'
        ]) }}
        {{ Form::text('new-venue[address][country]', Input::old('new-venue[address][country]'), [
            'class' => 'form-control',
            'placeholder' => 'Country (optional - "United Kingdom" by default)'
        ]) }}
    </div>
</div>  

<div class='form-group'>
    <label class="col-xs-4" for="new-venue[postcode]">Postcode <span class='redtext'>*</span></label>
    <div class='col-xs-8'>
        {{ Form::text('new-venue[address][postcode]', Input::old('new-venue[address][postcode]'), [
            'class' => 'form-control',
            'placeholder' => 'CV1 123',
            'required'
        ]) }}
    </div>
</div>  

<div class='form-group'>
    <label class="col-xs-4" for="new-venue[url]">Website</label>
    <div class='col-xs-8'>
        {{ Form::text('new-venue[url]', Input::old('new-venue[url]'), [
            'class' => 'form-control',
            'placeholder' => 'http://www.kasbahcoventry.co.uk'
        ]) }}
    </div>
</div>  

<div class='form-group'>
    <label class="col-xs-4" for="new-venue[extra-details]">Extra Details</label>
    <div class='col-xs-8'>
        {{ Form::text('new-venue[extra-details]', Input::old('new-venue[extra-details]'), [
            'class' => 'form-control',
            'placeholder' => 'e.g. No toilets, alcohol served, open until late'
        ]) }}
    </div>
</div>  

<div class='form-group'>
    <label class='col-xs-4' for='new-venue[image]'>Thumbnail Image</label>
    <div class='col-xs-8'>
        <input type='file' name='new-venue[image]' />
        Venue thumbnail - displayed on the homepage
    </div>
</div>