<div class="small-12 large-6 columns">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Change Gig Image</h3>
        </div>
        <div class="panel-body">
			{{ Form::open([
				'files' => true,
				'route' => [ 'admin.gigs.update-image', $gig->id ],
				'method' => 'POST'
			]) }}
			<div class="small-12 {{ $errors->first('date-time', 'image') }}">
				<label for='name'>Image <small>required</small></label>

				<input type='file' name='image' />
				{{ $errors->first('image', '<small class="error">:message</small>') }}
			</div>
			<div class="small-12">
				<input type="submit" class="button tiny success" />
			</div>
        </div>
    </div>
</div>