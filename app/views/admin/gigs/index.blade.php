@extends('layouts.admin')

@section('content')

    <h1 class='page-title'>Manage Gigs</h1>

    <div class='small-12'>
        <a class="button small small-12 medium-3" href="{{ route('admin.gigs.create') }}">Create Upcoming Gig</a>
    </div>

    <div class='small-12 columns panel'>
        <h2>Upcoming Gigs</h2>

        @if(count($upcomingGigs) < 1) 
            <div class='panel small-12 callout' style='margin-bottom: 0px; margin-top: 2em;'>
                There are no upcoming gigs.
            </div>
        @else
            @foreach($upcomingGigs as $gig)
                <a href='{{ route("admin.gigs.edit", [ "id" => $gig->id ]) }}'>
                <div class='small-6 large-4 columns'>
                <div class='panel callout columns' style='margin-bottom: 0px; margin-top: 2em;'>
                <img width='100%' src='{{ asset($gig->img) }}' />
                {{ $gig->name }}
                </div>
                </div>
                </a>
            @endforeach
        @endif
    </div>

    <div class='small-12 columns panel'>
        <h2>Past Gigs</h2>

        @if(count($pastGigs) < 1) 
            <div class='panel small-12 callout' style='margin-bottom: 0px; margin-top: 2em;'>
                There are no past gigs.
            </div>
        @else
            @foreach($pastGigs as $gig)
                <a href='{{ route("admin.gigs.edit", [ "id" => $gig->id ]) }}'>
                <div class='small-12 large-4 columns'>
                    <div class='panel callout columns' style='margin-bottom: 0px; margin-top: 2em;'>
                        <img width='100%' src='{{ asset($gig->img) }}' />
                        <div class='small-12 margin-t-15 align-center'>
                            <h3>{{ $gig->name }}</h3>
                            {{ $gig->venue->name }}<br />
                            {{ $gig->date_time->format('D jS M') }}
                        </div>
                    </div>
                </div>
                </a>
            @endforeach
        @endif
    </div>

@stop