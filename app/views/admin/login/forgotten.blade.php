@extends('layouts.admin')

@section('content')

	<h1 class='page-title'>Forgotten Your Password</h1>
	
	<div class='small-12 panel'>
	{{ Form::open(array('url' => 'admin/login/forgotten', 'method' => 'POST')) }}
		<label for='email'>Email</label>
		<input type='email' name='email' placeholder='Email' value='{{ Input::old("email") }}' />

		<button type="submit" class="button small"><i class="icon-check-sign"></i> Send Password Reset</button>
	</form>
	</div>
@stop