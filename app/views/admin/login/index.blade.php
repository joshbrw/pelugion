@extends('layouts.admin')

@section('content')
	<h1 class='page-title'>Sign In</h1>
	
	<div class='small-12 panel'>
		{{ Form::open(array('url' => 'admin/login', 'method' => 'POST', 'class' => 'form-horizontal')) }}
			<label for='email'>Email</label>
			<input type='email' name='email' placeholder='Email' value='{{ Input::old("email") }}' />

			<label for='password'>Password</label>
			<input type='password' name='password'  />

			<button type="" class="button small small-12"><i class="icon-check-sign"></i> Sign In</button>
		</form>
	</div>
@stop