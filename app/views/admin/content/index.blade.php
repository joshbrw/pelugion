@extends('layouts.admin')

@section('content')
    <h1 class='page-title'>Edit Site Content</h1>

    <p>On this page you can edit some of the content on the main site. Select a page (or template) from the tab bar below and then edit each available section on that page.</p>

    <div class='row'>
            <!-- Nav tabs -->
            <dl class="tabs small-12" data-tab>
                @foreach($pages as $page)
                    <dd><a href="#{{ Str::slug($page->name) }}" data-toggle="tab">{{ $page->name }}</a></dd>
                @endforeach
            </dl>
            
            <!-- Tab panes -->
            <div class="tabs-content small-12">
                <div class='content active'>Please choose a page/template to edit</div>
                @if($pages->count() == 0) 
                    <div>
                        There are no content sections to display.
                    </div>
                @else
                    @foreach($pages as $page)
                        <div class="content small-12" id="{{ Str::slug($page->name) }}">
                            <h2>{{ $page->name }}</h2>
                            <p>{{ $page->description }}</p>

                            @foreach($page->contentSections->toArray() as $section)

                                <div class='small-12 panel edit-content-field'" data-section-id="{{ $section['id'] }}" data-page-id="{{ $page->id }}"> 
                                    <h3 class="panel-title">{{ $section['title'] }}</h3>

                                    <p>{{ $section['description'] }}</p>
                                    
                                    <div class='save-fail-response--success alert-box success hidden'>Successfully updated.</div>
                                    <div class='save-fail-response--fail alert-box alert hidden'>There was an error.</div>
                                    <textarea class='content-textarea tinymce' rows="20">{{ $section['content'] }}</textarea>
                                    <br />
                                    <input type="button" value="Update" class="button success submit-button" />
                                </div>

                            @endforeach
                        </div>
                    @endforeach
                @endif
            </div>
    </div>
@stop

@section('scripts')
    @parent

    <script type="text/javascript">
        $(function() {  

            $('.edit-content-field input[type=button]').click(function(e) {
                e.preventDefault();

                var topParent = $(this).closest('div.edit-content-field'),
                    sectionId = topParent.attr('data-section-id'),
                    pageId = topParent.attr('data-page-id'),
                    contentInput = $(this).siblings('textarea.content-textarea'),
                    successResponse = $(this).siblings('.save-fail-response--success'),
                    failResponse = $(this).siblings('.save-fail-response--fail');

                $.ajax({
                    method: 'POST',
                    url: '{{ url('admin/content/update-section') }}',
                    dataType: 'json',
                    data: {
                        pageId: pageId,
                        sectionId: sectionId,
                        content: tinyMCE.activeEditor.getContent()
                    },
                    success: function(response) {
                        if(response.success == true) {
                            successResponse.fadeIn().delay(2000).fadeOut();
                        } else {
                            failResponse.fadeIn().delay(2000).fadeOut();
                        }
                    }
                });
            });
        });
    </script>
@stop