@extends('layouts.admin')

@section('content')
	<h1 class='page-title'>{{ $greeting }}, {{ Auth::user()->first_name }}!</h1>
	
	@if(Auth::user()->isSuperAdmin())
		<div class="small-12 medium-4 columns">
				<div class="caption">
					<h3>Current Users</h3>
					<p>There are currently <a href='{{ URL::to("admin/users") }}'>{{ $count_users }} users</a>.</p>
					<hr />
					<p>
						<a href="{{ url('admin/users') }}" class="button small small-12">View All Users</a>
					</p>
				</div>
		</div>

		<div class="small-12 medium-4 columns">
				<div class="caption">
					<h3>Activity Log</h3>
					<p>
						@foreach($log_items as $item)
							{{ $item->description }}<br />
							<small>{{ Time::parse($item->created_at)->diffForHumans() }}</small>.
							<hr />
						@endforeach
					</p>
					<p><a href="{{ url('admin/log') }}" class="button small small-12">View Full Log</a></p>
				</div>
		</div>
	@endif

	<div class="small-12 medium-4 columns">
			<div class="caption">
				<h3>Band Members</h3>
				<p>
					@foreach($band_members as $member)
						<img height='50px' width='50px' alt='{{ $member->full_name }}' title='{{ $member->full_name }}' src='{{ $member->getPhotoUrl() }}' />
					@endforeach
				</p>
			</div>		
	</div>


@stop