@extends('layouts.admin')

@section('content')

	<h1 class='page-title' class='page-title'>Create News Story</h1>
	
	<div class='large-12'>
		<div class='panel'>
			{{ Form::open(array('route' => 'admin.news.store', 'method' => 'POST', 'class' => 'form-horizontal'))}}

				<div class="small-12 {{ $errors->first('title', 'error') }}">
	                <label for='title'>Title <small>required</small></label>

	                <input type='text' name='title' required placeholder='Title' {{ $errors->first('title', "class='error'") }} />

	                {{ $errors->first('title', '<small class="error">:message</small>') }}
	            </div>

	            <div class="small-12 {{ $errors->first('subtext', 'error') }}">
	                <label for='subtext'>Subtext <small>required</small></label>

					<textarea name="subtext" class="tinymce {{ $errors->first('subtext', 'error') }}"  {{ $errors->first('subtext', "class='error'") }}></textarea>

	                {{ $errors->first('subtext', '<small class="error">:message</small>') }}
	            </div>

				<div class="small-12 {{ $errors->first('body', 'error') }}">
	                <label for='body'>Body <small>required</small></label>

					<textarea name="body" class="tinymce {{ $errors->first('body', 'error') }}" {{ $errors->first('body', "class='error'") }}></textarea>

	                {{ $errors->first('body', '<small class="error">:message</small>') }}
	            </div>

				<div class='small-12'>
					<label for='publish-now'>Publish this story now?</label>
					
					<input type='checkbox' checkedname='publish-now' {{ $errors->first('publish-now', "class='error'") }}/> 


	                {{ $errors->first('publish-now', '<small class="error">:message</small>') }}
				</div>
			
				<div class="form-group">
					<div class="col-lg-offset-5 col-lg-10">
						<button type="submit" class="btn btn-default"><i class="icon-check-sign"></i> Post News</button>
					</div>
				</div>
			</form>
		</div>
	</div>

@endsection