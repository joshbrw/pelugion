@extends('layouts.admin')	

@section('content')
	<h1 class='page-title'>News</h1>
	
	<a class="button small" href="{{ route('admin.news.create') }}">New News Story</a>

	@if(Auth::user()->isSuperAdmin())
		<a class="button small alert confirm-click" href="{{ route('admin.news.delete-all') }}">Delete ALL News</a>
	@endif

	<br />

	@if($news->count() == 0)
		There is no news stories to show. Use the above "New News Story" button to create a new one.
	@else
		@foreach(array_chunk($news->getItems(), 3) as $nChunk)
			<div class="small-12">
				@foreach($nChunk as $post)
					<div class="small-12 medium-4 columns">
						<a style='display: block;' href='{{ route("admin.news.edit", $post->id) }}'>
							<div class='panel 
								@if($post->published_at) 
									callout
								@endif
								dark-text'>
								<h3>{{ $post->title }}</h3>
								<h5 class='subtitle'>Posted by {{ $post->author->full_name }}</h5>
								<span class='label success'>Posted {{ Time::createFromTimestamp(strtotime($post->created_at))->diffForHumans() }}</span>

								@if($post->published_at)
									<span class='label success' title="This news story is visible on the homepage">Published!</span>
								@else
									<span class='label danger' title="This news story is not visible on the homepage">Not yet published!</span>
								@endif

								<p>{{ nl2br(strip_tags(shorten($post->subtext, 150))) }}</p>

								<a class='button primary small small-12' href='{{ route("admin.news.edit", $post->id) }}'>Edit</a>
								<a class='button danger small small-12 confirm-click' href='{{ route("admin.news.delete", $post->id) }}'>Delete</a>

								<br /><br />

								@if($post->published_at)
									<a class='button danger tiny small-12' href='{{ route("admin.news.unpublish", $post->id) }}'>Unpublish</a>
								@else
									<a class='button success tiny small-12' href='{{ route("admin.news.publish", $post->id) }}'>Publish</a>
								@endif

							</div>
						</a>
					</div>
				@endforeach
			</div>
		@endforeach
	@endif
	{{ $news->links() }}
@stop
