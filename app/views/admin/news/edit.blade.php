@extends('layouts.admin')

@section('content')

	<h1 class='page-title'>Edit News Story: {{ $post->title }}</h1>
	
	<div class='small-12'>
	<div class='panel'>
	{{ Form::open(array('url' => 'admin/news/'.$post->id.'', 'method' => 'PUT', 'class' => 'form-horizontal'))}}
		<div class='form-group'>
			<label for='title' class='col-lg-2 control-label'>Title <span class='redtext'>*</span></label>
			<div class='col-lg-10'>
				{{ Form::text('title', $post->title, [ 'class' => 'form-control' ]) }}
			</div>
		</div>
		<div class='form-group'>
			<label for='body' class='col-lg-2 control-label'>Subtext <span class='redtext'>*</span><br />(shown on the homepage)</label>
			<div class='col-lg-10'>
				{{ Form::textarea('subtext', $post->subtext, [ 'rows' => 5, 'class' => 'form-control' ] )}}
			</div>
		</div>
		<div class='form-group'>
			<label for='body' class='col-lg-2 control-label'>Body <span class='redtext'>*</span><br />(shown on the story page)</label>
			<div class='col-lg-10'>
				{{ Form::textarea('body', str_replace("<br />", "\n", $post->body), [ 'class' => 'form-control tinymce', 'style' => 'height: 400px;' ] )}}
			</div>
		</div>

		<label for='publish-now' class='col-lg-2 control-label'>Published</label>
		<input type='checkbox' class='form-control' name='publish-now' 
			@if($post->published_at != null)
				checked
			@endif
		/>
		If published now the news story will be visible on the homepage.
		
		<div class="form-group">
			<div class="col-lg-offset-5 col-lg-10">
				<button type="submit" class="btn btn-default"><i class="icon-check-sign"></i> Update News</button>
			</div>
		</div>
	</form>
	</div><!-- .panel -->
	</div><!-- .small-12 -->

@endsection