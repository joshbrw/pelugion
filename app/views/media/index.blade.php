@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/lightbox.css') }}" />
@stop

@section('main-content')
    <h1 class="section-header">Media</h1>
    <h2>Images</h2>

    <div class="band-photos">
        @foreach($images as $key => $i)
            <a href='{{ $i }}' data-lightbox='band-images'><img src='{{ $i }}' height='100px' width='100px' /></a>
        @endforeach
    </div>
@stop

@section('sidebar')
    @include('modules.random-ad')
@stop

@section('scripts')
    <script src="{{ asset('js/lightbox.min.js') }}" type="text/javascript"></script>
@stop