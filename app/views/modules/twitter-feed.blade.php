<section class='sidebar-module sidebar-module--twitter clearfix'>
    <h1 class='section-header'>Latest Tweets</h1>
    <a class="twitter-timeline" data-dnt="true" href="https://twitter.com/Pelugion" data-widget-id="420226501569568769">Tweets by @Pelugion</a>
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</section>