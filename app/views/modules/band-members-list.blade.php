<section class='sidebar-module sidebar-module--band-members clearfix'>
    <h1 class='section-header'>Band Members</h1>

    @foreach($bandMembers as $member)
        <a href='{{ $member->generateAboutUrl() }}' class='band-member'>
            <img class='band-member__avatar' width='50px' height='50px' src='{{ $member->getPhotoUrl() }}' />
            <div class='band-member__info'>
                <span class='band-member__info-name'>{{ $member->full_name }}</span>
                <span class='band-member__info-instruments'>{{ $member->getInstrumentsList() }}</span>
            </div>
        </a>
    @endforeach
</section>