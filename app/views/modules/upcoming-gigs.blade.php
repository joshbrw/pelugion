<section class='sidebar-module sidebar-module--gigs clearfix'>
    <h1 class='section-header'>Upcoming Gigs</h1>

    @if(count($upcomingGigs) == 0)
        <a href='#' class='gig'>
            <img class='gig__image' src='{{ Config::get('app.placeholder_path') }}' />
            <div class='gig__info'>
                <span class='gig__info-venue'>NO UPCOMING GIGS</span>
            </div>
        </a>
    @else
        @foreach($upcomingGigs as $gig) 
            <a href='{{ route("gigs.show", $gig->id) }}' class='gig'>
                <img class='gig__image' src='{{ $gig->getPhotoUrl() }}' />
                <div class='gig__info'>
                    <span class='gig__info-venue'>{{ $gig->venue->name }}</span>
                    <span class='gig__info-date'>{{ strtoupper($gig->date_time->format('jS F Y')) }}</span>
                </div>
            </a>
        @endforeach
    @endif
</section>