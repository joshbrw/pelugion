<!doctype html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <title>Pelugion</title>
    <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'> 
    <link rel='stylesheet' href='http://necolas.github.io/normalize.css/2.1.3/normalize.css' />
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,300' />
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Merriweather:400,700' />
    <link rel='stylesheet' href='{{ asset('css/main.css') }}' />
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" />
    <meta name="description" content="Pelugion are a heavy metal band from Coventry, UK." />
    <meta name="keywords" content="heavy metal, metal, rock, heavy, band, coventry, uk, pelugion"
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    @yield('styles')
</head>
<body>
    <div class='container'>
        <header class='main-header clearfix'>
            <div class='header__logo'>
                <img alt="Pelugion Logo" src='{{ asset("img/logo-light.svg") }}' />
            </div>
            <nav class='social clearfix'>
                <ul>
                    <li><a href='{{ route('facebook') }}'><img title="Pelugion's Facebook Page" alt="Facebook Logo" src='{{ asset("img/social/fb.png") }}' /></a></li>
                    <li><a href='{{ route('twitter') }}'><img title="Pelugion's Twitter Page"   alt="Twitter Logo" src='{{ asset("img/social/twitter.png") }}' /></a></li>
                </ul>
            </nav>
        </header>

        <nav class='main-nav clearfix'>
            <ul class='clearfix'>
                <li class='main-nav__button'><a href='{{ route('home') }}'>Home</a></li>
                <li class='main-nav__button'><a href='{{ route('about.index') }}'>About</a></li> 
                <li class='main-nav__button'><a href='{{ route('music.index') }}'>Music</a></li>
                <li class='main-nav__button'><a href='{{ route('media.index') }}'>Media</a></li>
                <li class='main-nav__button'><a href='{{ route('gigs.index') }}'>Gigs</a></li>
                <li class='main-nav__button'><a href='{{ route('contact.index') }}'>Contact</a></li>
            </ul>
        </nav>

        <section class='main-content clearfix'>
            @yield('main-content')
        </section>

        <section class='sidebar clearfix'>
            @yield('sidebar')
        </section>

        <footer class='footer'>
            <div class="footer__logo">
                <img height="50em" src="{{ asset('img/logo-light.svg') }}" />
            </div>
            <section class='footer__left'>
                <span class='contact-details'>
                    {{ $contactDetails }}
                </span>
            </section>
            <section class='footer__right'>
                <section class='created-by'>
                    Website Designed and Developed by <a href='http://www.joshbrown.me' target='_blank'>Josh Brown</a>.
                </section>
                <section class='copyright'>
                    Pelugion © {{ Time::now()->year }}.
                </section>
            </section>
        </footer>
        
        <script src='js/jquery.js'></script>
        <script src='js/main.js'></script> 
        @yield('scripts')     
    </div>

    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-46854688-1', 'pelugion.com');
    ga('send', 'pageview');

    </script>
</body>
</html>
