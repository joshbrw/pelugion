<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>{{ Config::get('band.name') }} - Admin Panel</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
	<link rel='stylesheet' href='{{ asset("admin-res/css/main.css") }}' />
	<link rel='stylesheet' href='{{ asset("admin-res/css/hint.min.css") }}' />
	
	<link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" />
</head>
<body>
	@if(Auth::check()) 
		@include('layouts.admin._nav-auth')
	@else
		@include('layouts.admin._nav-guest')
	@endif

	<br />
	<div class='large-12 row container'>
		@section('errors')
			@if(Session::has('success'))
				<br />
				<div data-alert class="alert-box success">
					<a href='#' class="close">&times;</a>
					{{ Session::get('success') }}
				</div>
			@endif
			@if(Session::has('error'))
				<br />
				<div data-alert class="alert-box danger">
					<a href='#' class="close">&times;</a>
					{{ Session::get('error') }}
				</div>
			@endif 

			@if(Session::has('info'))
				<br />
				<div data-alert class="alert-box info success">
					<a href='#' class="close">&times;</a>
					{{ Session::get('info') }}
				</div>
			@endif 

			@if(Auth::check())
				<br />
				@foreach(Auth::user()->notices as $notice)
					<div data-alert class="alert-box {{ ($notice->type == 'info') ? 'info' : 'danger' }} success">
						<a href='#' class="close">&times;</a>
						{{ $notice->notice }}
					</div>
					<?php $notice->delete() ?>
				@endforeach
			@endif

			{{-- @if($errors->any()) 
				<br />
				<div data-alert class="alert-box danger">
					<a href='#' class="close">&times;</a>
					The following validation errors occured:

					<ul>
						@foreach($errors->all('<li>:message</li>') as $error)
							{{ $error }}
						@endforeach
					</ul>
				</div>
			@endif --}}

			@if($errors->any())
				<div data-alert class="alert-box danger">
					<a href='#' class="close">&times;</a>
					Some validation errors occured. Please correct these and try again.
				</div>
			@endif

		@show
		
		@yield('content')
	</div>
	
	@section('scripts')
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="{{ asset('admin-res/js/foundation.min.js') }}"></script>
		<script src="{{ asset('admin-res/js/admin.js') }}"></script>
		<script src="{{ asset('admin-res/js/jquery.editable.js') }}"></script>
    	<script src="{{ asset('admin-res/tinymce/tinymce.min.js') }}"></script>

		<script>
			$(function() {
				$(document).foundation();

	            tinymce.init({
			        selector: '.tinymce',
			        mode : "none",
			        plugins : "anchor link lists",
			        menubar : false,
			        toolbar1: "bold italic underline | bullist numlist | link | styleselect",
			        style_formats : [
			            {title : 'Header 3', block: 'h3'},
			            {title : 'Header 4', block: 'h4'},
			            {title : 'Header 5', block: 'h5'}
			        ],
			        paste_auto_cleanup_on_paste : true,
			    });

	            //TODO: Remove before production

				console.log({{ json_encode(Session::all()) }});
			});
		</script>
	@show
</body>
</html>