<div class='fixed'>
<nav class="top-bar" data-topbar>
	<ul class="title-area">
		<li class="name">
		  <h1><a>Admin Panel</a></h1>
		</li>
		<li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
	</ul>

	<section class='top-bar-section'>
		<ul class='left'>
			<li 
					@if(ends_with(URL::current(), 'admin/dash'))
						class='active'
					@endif
			>
				<a href='{{ URL::to("admin/dash") }}'>Dashboard</a>
			</li>


			<li 
					@if(ends_with(URL::current(), 'admin/content'))
						class='active'
					@endif
			>
				<a href='{{ URL::to("admin/content") }}'>Pages &amp; Content</a>
			</li>
			

			<li 
					@if(ends_with(URL::current(), 'admin/posts'))
						class='active'
					@endif
			>
				<a href='{{ URL::to("admin/news") }}'>News</a>
			</li>

			<li 
					@if(ends_with(URL::current(), 'admin/gigs'))
						class='active'
					@endif
			>
				<a href='{{ URL::to("admin/gigs") }}'>Gigs</a>
			</li>

			@if(Auth::user()->isSuperAdmin())
				<li class="has-dropdown">
					<a href="#">Super Admin Tools <b class="caret"></b></a>

					<ul class="dropdown">
						<li><a href='{{ URL::to("admin/users") }}'>Manage Users</a></li>
						<li 
								@if(ends_with(URL::current(), 'admin/log'))
									class='active'
								@endif
						>
							<a href='{{ URL::to("admin/log") }}'>View Log</a>
						</li>
					</ul>
				</li>
			@endif
		</ul>

		<ul class="right">
			<li class="has-dropdown top-bar__user-info">
					<a href="#"><img height='30px' width='30px' class='top-bar__avatar' src='{{ Auth::user()->getPhotoUrl() }}' />Signed in as {{ Auth::user()->full_name }} <b class="caret"></b></a>

					<ul class="dropdown">

						<li 
								@if(ends_with(URL::current(), 'admin/account'))
									class='active'
								@endif
						>
							<a href='{{ URL::to("admin/account") }}'>Account &amp; Profile</a>
						</li>

						<li 
								@if(ends_with(URL::current(), 'admin/logout'))
									class='active'
								@endif
						>
							<a  href='{{ route("logout") }}'>Sign Out</a>
						</li>
					</ul>
				</li>
			@if(Session::has('switched_from'))
				<li class='has-form'>
					<a class='button alert' href='{{ url("admin/users/switch-back") }}'>Switch Back</a>
				</li>
			@endif
		</ul>
	</section>
</nav>
</div>