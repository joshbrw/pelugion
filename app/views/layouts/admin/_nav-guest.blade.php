<div class='fixed'>
<nav class="top-bar" data-topbar>
    <ul class="title-area">
        <li class="name">
          <h1><a>Admin Panel</a></h1>
        </li>
        <li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
    </ul>

    <section class='top-bar-section'>
        <ul class='left'>
            <li 
                    @if(ends_with(URL::current(), "admin/login"))
                        class='active'
                    @endif
                >
                    <a href='{{ URL::to("admin/login") }}'>Sign In</a>
                </li>
                <li 
                    @if(ends_with(URL::current(), "admin/login/forgotten"))
                        class='active'
                    @endif
                >
                    <a href='{{ URL::to("admin/login/forgotten") }}'>Forgotten Password</a>
                </li>
        </ul>
    </section>
</nav>
</div>