 <script type="text/javascript">

 var RecaptchaOptions = {
    theme : 'blackglass'
 };
 </script>
 <script type="text/javascript"
     src="http://www.google.com/recaptcha/api/challenge?k={{ Config::get('captcha.public_key') }}">
  </script>
  
  <noscript>
     <iframe src="http://www.google.com/recaptcha/api/noscript?k={{ Config::get('captcha.public_key') }}"
         height="300" width="500" frameborder="0"></iframe><br>
     <textarea name="recaptcha_challenge_field" rows="3" cols="40">
     </textarea>
     <input type="hidden" name="recaptcha_response_field"
         value="manual_challenge">
  </noscript>