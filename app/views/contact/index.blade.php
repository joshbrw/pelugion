@extends('layouts.master')

@section('main-content')
    <h1 class='clearfix section-header'>Contact Pelugion</h1>
    
    {{ $content }}

    {{-- @if($errors->all())
        <div class='errors'>
            The following errors occured:
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif --}}

    @if($errors->any()) 
        <div class='errors'>An error occured. Please fix this error and try again</div>
    @endif

    @if(Session::has('success'))
        <div class='success'>{{ Session::get('success') }}</div>
    @endif


    {{ Form::open([
        'method' => 'POST',
        'route' => 'contact.send',
        'class' => 'contact-form'
    ]) }}

    <div class='contact-form__control clearfix'>
        <label for="name" class='contact-form__label'>Your Name:</label>
        <input type='text' name="name" required placeholder="Joe Bloggs" value="{{ Input::old('name') }}" class='contact-form__input
            @if($errors->has('name'))
                error
            @endif
        ' />
        {{ $errors->first('name', "<div class='contact-form__field-error'>:message</div>") }}
    </div>

    <div class='contact-form__control clearfix'>
        <label for="name" class='contact-form__label'>Your Email:</label>
        <input type='email' value="{{ Input::old('email') }}" name="email"  required placeholder="joe@bloggs.com" class='contact-form__input
            @if($errors->has('email'))
                error
            @endif
        ' />
        {{ $errors->first('email', "<div class='contact-form__field-error'>:message</div>") }}
    </div>

    <div class='contact-form__control clearfix'>
        <label for="message" class='contact-form__label'>Your Message:</label>
        <textarea name="message"  cols="50" rows="10" class="contact-form__input
            @if($errors->has('message'))
                error
            @endif        
        " placeholder="Your message to Pelugion">{{ Input::old('message') }}</textarea>
        {{ $errors->first('message', "<div class='contact-form__field-error'>:message</div>") }}
    </div>

    <div class='contact-form__control clearfix'>
        <div class="contact-form__input--offset">
            @include('contact._partials.captcha')
        {{ $errors->first('captcha', "<div class='contact-form__field-error'>:message</div>") }}
        </div>
    </div>

    <div class='contact-form__control clearfix'>
        <input type="submit" value="Send Message" class="btn contact-form__input--offset contact-form__submit" />
    </div>

    {{ Form::close() }}
   
@stop


@section('sidebar')
    @include('modules.twitter-feed')
@stop