<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Coming Soon</title>
	<link href='http://fonts.googleapis.com/css?family=Merriweather:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="{{ asset('css/temp.css') }}" />
</head>
<body>
	<div id="fb-root"></div>

	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=563359067058454";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

	<div class="container">
		<img class="logo" src="{{ asset('img/logo.png') }}" alt="Pelugion - A heavy metal band from Coventry, UK" /><br />
		<h1 class="sorry">
			Sorry!
		</h1>
		<h2 class='coming-soon'>
			The Pelugion website is coming soon.
		</h2>	
		<div class="fb-like-box" data-href="https://www.facebook.com/pages/Pelugion/1428073117412979" data-width="100" data-colorscheme="dark" data-show-faces="false" data-header="false" data-stream="false" data-show-border="false"></div>
	</div>

	<script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-46854688-1']);
        _gaq.push(['_trackPageview']);
        (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; 

        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';

        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
</body>
</html>