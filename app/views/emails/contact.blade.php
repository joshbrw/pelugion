<h1>New Contact Message</h1>

A new contact message has arrived via the contact form on the website. It follows:<br /><br />

<strong>Name:</strong> {{ $name }}<br />
<strong>Email:</strong> {{ $email }}<br /><br />
<strong>Message:</strong><br />
{{ $messageStr }}<br /><br />

This message was sent from {{ $ip }}