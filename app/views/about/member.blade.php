@extends('layouts.master')

@section('main-content')
    <h1 class='clearfix section-header'>{{ $member->full_name}}


    @if($member->nickname) 
        ({{$member->nickname}})
    @endif</h1>

    <div class='member__image'>
        <img alt="{{ $member->full_name }}" width="200px"   title="{{ $member->full_name }}" src='{{ $member->getPhotoUrl() }}' />
    </div>

    <div class='member__details'>
        <strong>Name:</strong> {{ $member->full_name }}<br />
        <strong>Nickname:</strong> {{ $member->nickname }}<br />
        <strong>Born:</strong> {{ $member->date_of_birth->format('jS F Y')  }}<br />
        <strong>Plays:</strong> {{ $member->getInstrumentsList() }}<br /><br />
        <strong>Bio:</strong><br />
        @if($member->bio)
            {{ $member->bio }}
        @else
            This member has not filled out their bio yet.
        @endif
        <br /><br />  

        <strong>Interests:</strong><br />
        @if($member->interests)
            {{ $member->interests }}
        @else
            This member has not filled out their interests yet.
        @endif
        <br /><br />

        <strong>Influences:</strong><br />
        @if($member->influences)
            {{ $member->influences }}
        @else
            This member has not filled out their influences yet.
        @endif
        <br /><br />
    </div>
@stop

@section('sidebar')
    <section class='sidebar-module sidebar-module--band-members clearfix'>
        <h1 class='section-header'>Band Members</h1>

        @foreach($bandMembers as $member)
            <a href='{{ route("about.user", [ $member->id, Str::slug($member->full_name) ]) }}' class='band-member'>
                <img class='band-member__avatar' width='50px' height='50px' src='{{ $member->getPhotoUrl() }}' />
                <div class='band-member__info'>
                    <span class='band-member__info-name'>{{ $member->full_name }}</span>
                    <span class='band-member__info-instruments'>{{ $member->getInstrumentsList() }}</span>
                </div>
            </a>
        @endforeach
    </section>
@stop