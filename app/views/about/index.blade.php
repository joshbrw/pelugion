@extends('layouts.master')

@section('main-content')
    <h1 class='clearfix section-header'>About Pelugion</h1>
    <section class='cover-photo clearfix'>
        <img src='{{ asset("img/temp-about-cover.png") }}' />
    </section>
    {{ $content }}
@stop

@section('sidebar')
    @include('modules.band-members-list')
@stop