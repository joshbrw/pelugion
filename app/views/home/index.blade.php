@extends('layouts.master')

@section('main-content')
	<section class='cover-photo clearfix'>
		<img src='{{ asset("img/temp-cover-photo.png") }}' />
	</section>

	<h1 class='clearfix section-header'>Latest News</h1>

	@if($news->count() == 0) 
		<article class='no-news'>
			There are currently no news stories.
		</article>
	@else
		@foreach($news as $post)
			<article class='news-story'>
			<h2 class='news-story__title'>
				<a href='{{ route("news.show", [$post->id]) }}'>{{ $post->title }}</a>
			</h2>
			<div class='news-story__meta'>
				Posted on {{ $post->published_at->format('l jS M Y') }} - by <a class='news-story__author' href='{{ $post->author->generateAboutUrl() }}'>{{ $post->author->full_name }}</a>
			</div>
			<div class='news-story__body'>
				{{ trim($markdownParser->transformMarkdown($post->getSubtext())) }}
			</div>
			<a class='news-story__read-more btn' href='{{ route('news.show', ['id' => $post->id, 'slug' =>$post->getSlug()]) }}'>Read More</a>
			</article>

		@endforeach
	@endif
@stop


@section('sidebar')
	@include('modules.upcoming-gigs')

	
    @include('modules.twitter-feed')
@stop