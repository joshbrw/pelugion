<?php

use Illuminate\Database\Migrations\Migration;

class CreateInstrumentUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('instrument_user', function($table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('instrument_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('instrument_user');
	}

}