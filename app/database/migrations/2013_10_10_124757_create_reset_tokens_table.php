<?php

use Illuminate\Database\Migrations\Migration;

class CreateResetTokensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reset_tokens', function($table) {
			$table->increments('id');
			$table->text('token');
			$table->integer('user_id')->index()->references('id')->on('users');
			$table->datetime('expires');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reset_tokens');
	}

}