<?php

use Illuminate\Database\Migrations\Migration;

class CreatePhotoAlbumsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('photo_albums', function($table) {
			$table->increments('id');
			$table->string('title');

			/**
			 * e.g. If an album relates to a gig or a venue with a specific id
			 */
			$table->string('object_type')->nullable();
			$table->integer('object_id')->nullable();

			$table->integer('author_id');
			$table->boolean('visible')->default(1);
			$table->boolean('deleteable')->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('photo_albums');
	}

}