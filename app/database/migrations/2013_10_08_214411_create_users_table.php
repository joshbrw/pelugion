<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table) {
			$table->increments('id');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('nickname')->nullable();
			$table->string('email');
			$table->string('password');
			$table->datetime('date_of_birth');
			$table->integer('role')->default(0);
			$table->boolean('band_member')->default(0);
			$table->text('avatar_url')->nullable();
			$table->text('bio')->nullable();
			$table->text('interests')->nullable();
			$table->text('influences')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}