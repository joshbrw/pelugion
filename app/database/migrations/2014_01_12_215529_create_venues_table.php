<?php

use Illuminate\Database\Migrations\Migration;

class CreateVenuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('venues', function($table) {
			$table->increments('id');
			$table->string('name');
			$table->text('website')->nullable();
			$table->text('extra_details')->nullable();

			$table->string('addr_first_line');
			$table->string('addr_second_line')->nullable();
			$table->string('addr_city');
			$table->string('addr_county')->nullable();
			$table->string('addr_country');
			$table->string('addr_postcode');
			$table->string('latitude')->nullable();
			$table->string('longitude')->nullable();

			$table->string('avatar_url')->nullable();

			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('venues');
	}

}