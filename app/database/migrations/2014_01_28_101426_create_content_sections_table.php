<?php

use Illuminate\Database\Migrations\Migration;

class CreateContentSectionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('content_sections', function($table) {
			$table->increments('id');
			$table->string('title');
			$table->string('description');
			$table->text('content');
			$table->string('identifier');
			$table->integer('page_id');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('content_sections');
	}

}