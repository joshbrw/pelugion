<?php

use Illuminate\Database\Migrations\Migration;

class CreateGigsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gigs', function($table) {
			$table->increments('id');
			$table->string('name');
			$table->text('description')->nullable();
			$table->datetime('date_time');
			$table->string('age_rating');
			$table->float('ticket_price');
			$table->integer('venue_id');
			$table->string('image_url')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gigs');
	}

}