<?php

	class UsersTableSeeder extends Seeder {

		public function run() {
			User::truncate();

			User::create([
				'first_name' => 'Josh',
				'last_name' => 'Brown',
				'email' => 'josh@joshbrown.me',
				'password' => Hash::make('QWJwkuT4fyz9X9fd2'),
				'date_of_birth' => Time::parse('9th march 1995'),
				'role' => 1,
				'band_member' => false
			]);

			User::create([
				'first_name' => 'Brandon',
				'last_name' => 'Balou',
				'nickname' => 'The Vulture',
				'email' => 'brandonbalou@gmail.com',
				'password' => Hash::make('Q3n9XAFeU9XzZsfM9'),
				'date_of_birth' => Time::parse('1st january 1970'),
				'role' => 0,
				'band_member' => true
			]);

			User::create([
				'first_name' => 'Andy',
				'last_name' => 'Sweeney',
				'nickname' => 'The Quatch',
				'email' => 'andy_pandy2k6@hotmail.com',
				'password' => Hash::make('MRRnZxU6t7HRv8p8D'),
				'date_of_birth' => Time::parse('1st january 1970'),
				'role' => 0,
				'band_member' => true
			]);

			User::create([
				'first_name' => 'John',
				'last_name' => 'Pittaway',
				'nickname' => 'The Goat',
				'email' => 'j.pittaway95@gmail.com',
				'password' => Hash::make('KFEC8cPQkH32x3suA'),
				'date_of_birth' => Time::parse('1st january 1970'),
				'role' => 0,
				'band_member' => true
			]);

		}
	}