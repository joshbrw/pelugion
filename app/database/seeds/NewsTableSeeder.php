<?php

	class NewsTableSeeder extends Seeder {

		public function run() {
			News::truncate();

			$faker = \Faker\Factory::create();
			
			foreach(range(1,30) as $index) {
				News::create([
					'author_id' => rand(1,2),
					'title' => $faker->sentence,
					'subtext' => $faker->paragraph,
					'body' => $faker->paragraphs(3, true)
				]);
			}

		}

	}