<?php

    class PhotoAlbumSeeder extends Seeder {

        public function run() {
            foreach(range(1, 3) as $i) {
                PhotoAlbum::create([
                    'title' => 'An Album about Venue '.$i,
                    'object_type' => 'Venue',
                    'object_id' => $i,
                    'author_id' => User::find(1)->id,
                    'visible' => 1
                ]);
            }

            foreach(range(1, 3) as $i) {
                PhotoAlbum::create([
                    'title' => 'An Album about Gig '.$i,
                    'object_type' => 'Gig',
                    'object_id' => $i,
                    'author_id' => User::find(1)->id,
                    'visible' => 1
                ]);
            }
        }

    }