<?php
    
    class VenueSeeder extends Seeder {

        public function run() {
            Venue::truncate();

            $faker = \Faker\Factory::create();

            foreach(range(1, 10) as $i) {
                Venue::create([
                    'name' => $faker->company,
                    'extra_details' => null,
                    'addr_first_line' => $faker->streetAddress,
                    'addr_second_line' => $faker->secondaryAddress,
                    'addr_city' => $faker->city,
                    'addr_county' => $faker->state,
                    'addr_country' => $faker->country,
                    'latitude' => $faker->latitude,
                    'longitude' => $faker->longitude
                ]);
            }
        }

    }