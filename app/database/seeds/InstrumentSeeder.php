<?php

	class InstrumentSeeder extends Seeder {

		public function run() {

			Instrument::truncate();

			$instruments = array('Vocals', 'Guitar', 'Drums', 'Bass');

			foreach($instruments as $instrument) {
				$i = new Instrument;
				$i->name = $instrument;
				$i->save();
			}
		}
	}