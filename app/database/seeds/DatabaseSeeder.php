<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UsersTableSeeder');
		$this->call('InstrumentSeeder');
		$this->call('NewsTableSeeder');
		$this->call('VenueSeeder');
		$this->call('GigSeeder');
		$this->call('PhotoAlbumSeeder');
	}

}