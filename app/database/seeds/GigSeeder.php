<?php
    
    class GigSeeder extends Seeder {

        public function run() {
            Gig::truncate();

            $faker = \Faker\Factory::create();
            $age_ratings = [ '18+', 'Any Age', '16-18' ];
            foreach(range(1,10) as $i) {
                Gig::create([
                    'name' => $faker->sentence(2),
                    'description' => $faker->sentence(10),
                    'date_time' => $faker->dateTimeBetween('now', '+1 year'),
                    'age_rating' => $age_ratings[array_rand($age_ratings)],
                    'venue_id' => Venue::orderBy(DB::raw('RAND()'))->first()->id,
                ]);
            }
        }
    }