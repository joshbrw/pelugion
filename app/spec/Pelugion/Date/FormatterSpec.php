<?php

namespace spec\Pelugion\Date;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class FormatterSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Pelugion\Date\Formatter');
    }
}
