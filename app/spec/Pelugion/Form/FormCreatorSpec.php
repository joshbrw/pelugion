<?php

namespace spec\Pelugion\Form;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class FormCreatorSpec extends ObjectBehavior
{
    function let() {
        $this->beInitializedWith('1');
    }
    function it_is_initializable()
    {
        $this->shouldHaveType('Pelugion\Form\FormCreator');
    }
}
