<?php

namespace spec\Pelugion\Pagination;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class FoundationPresenterSpec extends ObjectBehavior
{
    private $paginator;

    /**
     * Bootstrap the class and inject in the paginator needed
     * 
     * @param  IlluminatePaginationPaginator $paginator Laravel's Paginator
     */
    function let(\Illuminate\Pagination\Paginator $paginator)
    {
        $this->paginator = $paginator;
        $this->beConstructedWith($paginator);
    }

    function it_is_initializable()
    {
        $this->shouldBeAnInstanceOf('Pelugion\Pagination\FoundationPresenter');
    }

    function it_should_return_page_link_wrapper() {
        $url = "http://www.google.com";
        $page = "Page Name";
        $this->getPageLinkWrapper($url, $page)->shouldReturn('<li><a href="'.$url.'">'.$page.'</a></li>');
    }

    function it_should_return_disabled_link_wrapper() {
        $text = "Page Name";
        $this->getDisabledTextWrapper($text)->shouldReturn('<li class="disabled">' . $text . '</li>');
    }

    function it_should_return_active_link_wrapper() {
        $text = "Page Name";
        $this->getActivePageWrapper($text)->shouldReturn('<li class="current"><a>'.$text.'</a></li>');
    }

    /**
     * Finish the test
     */
    function letgo() {
        unset($this->paginator);
    }
}
