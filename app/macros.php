<?php

Form::macro('textField', function($name, $label, $default = null, $errors = null, $options = []) {
    $html =  '<div class="small-12 ' . (!$errors) ?: $errors->first($name, "error") . '">';
    $html .= '<label for="' . $name . '">' . $label;

    if(isset($options['required']) && $options['required']) {
        $html .= ' <small>required</small>';
    }

    $html .= '</label>';
    $html .= '<input type="text" name="' . $name . '" ';

    if(isset($options['placeholder'])) {
        $html .= 'placeholder="' . $options['placeholder'] . '" ';
    }

    if($default !== null) {
        $html .= 'value="' . $default . '"';
    }

    $html .= ' ' . (!$errors) ?: $errors->first($name, "class='error'") . ' />';
    $html .= (!$errors) ?: $errors->first($name, '<small class="error">:message</small>');
    $html .= '</div>';

    return $html;
});