<?php namespace Pelugion\Date;

class Formatter {

    /**
     * Create a datetime-local value from a date string
     * @param  string $date Date string
     * @return string       Formatted string
     */
    public static function forDateTimeLocal($date) {
        $c = \Carbon\Carbon::parse($date);

        return $c->format('Y-m-d') . "T" . $c->format('H:i:s.u');
    }
}