<?php namespace Pelugion\Repositories\Gig;

class EloquentGigRepository implements GigRepository {
    public function find($id) {
        return \Gig::find($id);
    }
}