<?php namespace Pelugion\Repositories\Gig;

interface GigRepository {
    public function find($id);
}