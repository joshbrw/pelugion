<?php namespace Pelugion\Greeting;

use \Carbon\Carbon as Time;

class Generator {

    private static $greetings = [ 
        '5, 11' => 'Good Morning',
        '11, 17' => 'Good Afternoon',
        '18, 23' => 'Good Evening',
        '0, 4' => 'Good Night'
    ];

    private static $default = 'Hello';

    public static function generate() {
        $returnGreeting = false;
        $currentTime = new Time;

        foreach(self::$greetings as $times => $greeting) {
            $splitTimes = explode(', ', $times);
            foreach(range($splitTimes[0], $splitTimes[1]) as $time) {
                if($time == $currentTime->hour) $returnGreeting = $greeting;
            }
        }

        if(!$returnGreeting) $returnGreeting = self::$default;

        return $returnGreeting;
    }

}