<?php
    
    namespace Pelugion\Interfaces;

    interface AddressableInterface {

        public function getAddressString();
        
    }