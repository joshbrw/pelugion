<?php

    namespace Pelugion\Interfaces;

    interface DisplayPhotoInterface {

        public function hasPhoto();
        public function getPhotoPath();
        public function getPhotoUrl();
        
    }