<?php namespace Pelugion\Interfaces;

interface SluggableInterface {

    public function getSlug();
    
}