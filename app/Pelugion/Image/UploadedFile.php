<?php namespace Pelugion\Image;

class UploadedFile {

    private $image;
    private $path;

    /**
     * Constructor
     * @param InterventionImageImage $image The image object
     * @param string                 $path  The filename
     */
    public function __construct(\Intervention\Image\Image $image, $path) {
        $this->image = $image;
        $this->path = $path;
    }

    public function getImage() {
        return $this->image;
    }

    public function getPath() {
        return $this->path;
    }

}