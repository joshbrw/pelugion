<?php namespace Pelugion\Image;

use Image;
use Config;

class Uploader {

    public static function upload(\Symfony\Component\HttpFoundation\File\UploadedFile $file, $width = null, $height = null)
    {
        $path = substr(md5(time() . $file->getClientOriginalName()), 10) . '.' . $file->getClientOriginalExtension();

        $img = Image::make($file->getRealPath());

        if($width && $height) {
            $img->resize($width, null, true);
            $img->crop($width, $height);
        }
        
        $i = $img->save(Config::get('app.image_upload_path') . $path);

        return new \Pelugion\Image\UploadedFile($i, $path);
    }

}