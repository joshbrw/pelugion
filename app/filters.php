<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{

	View::share('markdownParser', new MarkdownParser);

	$footerContactDetails = ($sect = ContentSection::findSection('template.contact-details')) ? $sect->content : 'contact@pelugion.com';
	View::share('contactDetails', $footerContactDetails);


	ResetToken::where('expires', '<=', Time::now())->delete();
	

	Event::listen('user.login', 'LogHandler@login');
	Event::listen('user.failed-login', 'LogHandler@failedLogin');
	Event::listen('user.logout', 'LogHandler@logout');

	Event::listen('user.account-update', 'LogHandler@accountUpdate');
	Event::listen('user.password-update', 'LogHandler@passwordUpdate');
	Event::listen('user.avatar-update', 'LogHandler@avatarUpdate');
	Event::listen('user.account-update', 'LogHandler@accountUpdate');


	Event::listen('user.update-other-account', 'LogHandler@updateOtherAccount');
	Event::listen('user.switch-account', 'LogHandler@switchAccount');
	Event::listen('user.switch-back', 'LogHandler@switchBack');
	Event::listen('user.request-password-reset', 'LogHandler@requestPasswordReset');
	Event::listen('user.reset-password', 'LogHandler@resetPassword');
	Event::listen('news.publish', 'LogHandler@publishNews');
	Event::listen('news.unpublish', 'LogHandler@unpublishNews');
	Event::listen('news.post', 'LogHandler@postNews');

	Event::listen('gig.create', 'LogHandler@createGig');
});


App::after(function($request, $response)
{
	//
});


App::error(function(Exception $e) {
	if(str_contains(URL::current(), 'admin')) {
		if($e instanceof Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
			return Redirect::to('admin/dash')->with('error', 'That page does not exist');
		}
	} else {
		if($e instanceof Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
			return View::make('errors.404');
		}
	}
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest()) {
		return Redirect::to('admin/login');
	}
});

Route::filter('auth.superadmin', function()
{
	if(!Auth::check()) {
		return Redirect::to('admin/login');
	}
	if(!Auth::user()->isSuperAdmin()){
		return Redirect::to('admin/dash')->with('error', trans('messages.not-super-admin'));
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::route('dash');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});