<?php

class MediaController extends BaseController {

    public function index()
    {
        $imgs = [];

        foreach(glob(public_path() . "/img/band-photos/*.JPG") as $img) {
            $explodedImg = explode('/', $img);
            $lastPart = $explodedImg[ count($explodedImg) - 1 ];
            $imgs[] = asset('/img/band-photos/' . $lastPart);
        }
        return View::make('media.index')->with('images', $imgs);
    }

}