<?php

    class ContactController extends BaseController {

        /**
         * Show the form to send a contact message
         * @return Response
         */
        public function index(){
            $displayAboveContactForm = ($sect = get_content('contact.main')) ? $sect->content : 'Contact Pelugion using the form below.';

            return View::make('contact.index')
                    ->with('content', $displayAboveContactForm);
        }

        /**
         * Send the contact message
         * @return Response 
         */
        public function send(){

            $client = new \Guzzle\Http\Client('http://www.google.com/recaptcha/api');

            // Create a GET request using Relative to base URL
            // URL of the request: http://baseurl.com/api/v1/path?query=123&value=abc)
            $request = $client->post('verify', [], [
                'privatekey' => Config::get('captcha.private_key'),
                'remoteip' => $_SERVER['REMOTE_ADDR'],
                'challenge' => Input::get('recaptcha_challenge_field'),
                'response' => Input::get('recaptcha_response_field')
            ]);

            $response = $request->send();

            $lines = explode("\n", $response->getBody(true));

            if($lines[0] != 'true') {
                /**
                 * Captcha failed, go back with captcha error
                 */
                return Redirect::back()->withInput()->withErrors([ 'captcha' => trans('messages.invalid-captcha') ]);
            }

            $validator = Validator::make(Input::all(), [
                'name' => 'required|max:70',
                'email' => 'required|email',
                'message' => 'required|max:300'
            ]);

            if($validator->fails()) {
                return Redirect::route('contact.index')->withInput()->withErrors($validator);
            }

            /**
             * All things in the world are good, lets get sending some emails eh?
             */
            
            $data = [
                'name' => Input::get('name'),
                'email' => Input::get('email'),
                'messageStr' => nl2br(Input::get('message')),
                'ip' => $_SERVER['REMOTE_ADDR']
            ];

            Mail::send('emails.contact', $data, function($msg) {
                $msg->subject('New Contact Message - Pelugion.com');
                $msg->to('josh@joshbrown.me');
            });

            return Redirect::route('contact.index')->with('success', ContentSection::findSection('contact.success')->content);

        }
    }