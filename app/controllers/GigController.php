<?php

class GigController extends BaseController {

    public function index()
    {
        return View::make('gigs.index');
    }

    public function show($id, $slug = null) 
    {
        try {
            $gig = Gig::findOrFail($id);
        } catch(Exception $e) {
            return App::abort(404);
        }

        if($slug === null) {
            return Redirect::route('gigs.show', [ $gig->id, $gig->getSlug() ]);
        }

        return View::make('gigs.show')->with('gig', $gig);
    }
}