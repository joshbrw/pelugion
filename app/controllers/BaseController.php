<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}

		$this->shareUpcomingGigs();
		$this->shareBandMembers();
		$this->shareRandomAdverts();
	}

	public function shareUpcomingGigs()
	{
		View::share('upcomingGigs', Gig::with('venue')->upcoming()->orderBy('date_time', 'ASC')->get());
	}

	public function shareBandMembers()
	{
		View::share('bandMembers', User::bandMembers()->get());
	}

	public function shareRandomAdverts() {
		$adverts = [];

        foreach(glob(public_path() . "/img/adverts/*") as $img) {
            $explodedImg = explode('/', $img);
            $lastPart = $explodedImg[ count($explodedImg) - 1 ];
            $adverts[] = asset('/img/adverts/' . $lastPart);
        }

        // dd(array_rand($adverts));
        View::share('randomAdvert', $adverts[array_rand($adverts)]);
	}

}