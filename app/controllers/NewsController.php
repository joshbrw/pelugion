<?php
    
    class NewsController extends BaseController {

        public function show($id, $slug = null) {
            if(!$post = News::find($id)) return Redirect::route('home');
            if($slug != $post->getSlug()) return Redirect::route('news.show', [ $post->id, $post->getSlug() ]);

            return View::make('news.show')->with('post', $post)
                    ->with('gigs', Gig::getUpcomingGigs()->paginate(3));
        }
        
    }