<?php

	class AboutController extends BaseController {

		public function showUser($id, $slug) {
			if(!$user = User::with('instruments')->find($id)) return Redirect::route('about.index');
			if(!$user->isBandMember()) return Redirect::route('about.index'); 
			if($slug != $user->getNameSlug()) return Redirect::route('about.user', [ 'id'=>$user->id, 'slug'=>$user->getNameSlug() ]);

			return View::make('about.member')
						->with('member', $user)
						->with('bandMembers', User::bandMembers()->get());
			
		}

		public function index() {
			$infoSection = ($sect = get_content('about.main')) ? $sect->content : 'This has not yet been filled out';


			return View::make('about.index')
						->with('bandMembers', User::bandMembers()->get())
						->with('content', $infoSection);
		}
	}