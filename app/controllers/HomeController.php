<?php

class HomeController extends BaseController {

	public function index()
	{
		return View::make('home.index')
			->with('news', News::with('author')->published()->orderBy('published_at', 'desc')->paginate(3))
            ->with('gigs', Gig::getUpcomingGigs()->paginate(3));
	}

}