<?php

	class AdminNewsController extends BaseController {

		/**
		 * List all posts
		 * If user is admin then show all posts
		 * otherwise show posts by that use
		 * @return View
		 */
		public function index() {
			$news = null;

			if(Auth::user()->isSuperAdmin()) {
				$news = News::orderBy('created_at', 'desc')->paginate(10);
			} else {
				$news = Auth::user()->news()->orderBy('created_at', 'desc')->paginate(10);
			}

			return View::make('admin.news.index', [
				'news' => $news
			]);
		}

		/**
		 * Show a single post
		 * @param int $id Post ID
		 * @return View
		 */
		public function show($id) {
			if(!$news = News::find($id))
				return Redirect::route('admin.news.index')->with('error', trans('messages.not-found', ['object'=>'News']));
		}

		/**
		 * Show the form to create a post
		 * @return View
		 */
		public function create() {
			return View::make('admin.news.create');
		}

		/**
		 * Store a post
		 * @return Redirect 
		 */
		public function store() {
			$validator = Validator::make(Input::all(), Config::get('validation.news.create'));

			if($validator->fails()) {
				return Redirect::back()->withInput()->withErrors($validator);
			}

			$post = new News;
			$post->title = Input::get('title');
			$post->subtext = Input::get('subtext');
			$post->body = Input::get('body');
			$post->published_at = (Input::has('publish-now')) ? Time::now() : null;
			Auth::user()->news()->save($post);

			Event::fire('news.post', $post);

			return Redirect::back()->with('success', trans('messages.object-created',['object'=>'News Story']));
		}

		/**
		 * Show the form to edit a post
		 * @param  int $id Post ID
		 * @return View     
		 */
		public function edit($id) {
			if(!$news = News::find($id)) return Redirect::route('admin.news.index')->with('error', trans('messages.not-found', ['object'=>'News Story']));
			if(!Auth::user()->hasAccessTo($news)) return Redirect::route('admin.news.index')->with('error', trans('messages.not-authorized'));

			return View::make('admin.news.edit')->with('post', $news);
		}

		public function update($id) {
			if(!$news = News::find($id)) return Redirect::route('admin.news.index')->with('error', trans('messages.not-found', ['object'=>'News Story']));
			if(!Auth::user()->hasAccessTo($news)) return Redirect::route('admin.news.index')->with('error', trans('messages.not-authorized'));

			$validator = Validator::make(Input::all(), Config::get('validation.news.create'));

			if($validator->fails()) {
				return Redirect::back()->withInput()->withErrors($validator);
			}

			if(Input::has('publish-now')) 
				$news->published_at = Time::now();

			elseif(Input::has('publish-now') != true)
				$news->published_at = null;

			foreach(Input::only('title', 'subtext', 'body') as $key => $value) {
				$news->{$key} = $value;
			}

			$news->save();
			return Redirect::back()->with('success', trans('messages.object-updated', ['object'=>'News Post']));

		}

		public function delete($id) {
			if(!$news = News::find($id)) return Redirect::route('admin.news.index')->with('error', trans('messages.not-found', ['object'=>'News Story']));
			if(!Auth::user()->hasAccessTo($news)) return Redirect::route('admin.news.index')->with('error', trans('messages.not-authorized'));

			$news->delete();

			return Redirect::route('admin.news.index')->with('success', trans('messages.object-deleted', ['object'=>'News Story']));
		}

		/**
		 * Publish a post - sets it's published_at to the current time
		 * @param  int $id Post ID
		 * @return View
		 */
		public function publish($id)
		{
			if(!$news = News::find($id)) return Redirect::route('admin.news.index')->with('error', trans('messages.not-found', ['object'=>'News Story']));
			if(!Auth::user()->hasAccessTo($news)) return Redirect::route('admin.news.index')->with('error', trans('messages.not-authorized'));

			$news->published_at = Time::now();
			$news->save();

			Event::fire('news.publish', $news);

			return Redirect::route('admin.news.index')->with('success', trans('messages.news-published'));
		}

		/**
		 * Unpublis ha post - sets it's published_at to null
		 * @param  int $id Post ID
		 * @return View
		 */
		public function unpublish($id)
		{
			if(!$news = News::find($id)) return Redirect::route('admin.news.index')->with('error', trans('messages.not-found', ['object'=>'News Story']));
			if(!Auth::user()->hasAccessTo($news)) return Redirect::route('admin.news.index')->with('error', trans('messages.not-authorized'));

			$news->published_at = null;
			$news->save();


			Event::fire('news.unpublish', $news);


			return Redirect::route('admin.news.index')->with('success', trans('messages.news-unpublished'));
		}

		public function deleteAll() {
			News::truncate();

			return Redirect::back()->with('success', trans('messages.all-news-deleted'));
		}
	}