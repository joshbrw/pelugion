<?php

    class AdminAjaxController extends BaseController {

        public function getAllVenues() {
            return Response::json(Venue::orderBy('name')->get());
        }

    }