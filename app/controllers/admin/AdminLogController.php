<?php

	class AdminLogController extends BaseController {

		public function index() {
			return View::make('admin.log.index')->with('log_items', LogItem::orderBy('created_at', 'desc')->paginate(15));
		}

	}