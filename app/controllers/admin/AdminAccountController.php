<?php

	class AdminAccountController extends BaseController {

		/**
		 * Show account editing form
		 * @return View
		 */
		public function index() {
			$months = [
				'January',
				'February',
				'March',
				'April',
				'May',
				'June',
				'July',
				'August',
				'September',
				'October',
				'November',
				'December'
			];
			return View::make('admin.account.edit')->with('user', Auth::user())
													->with('instruments', Instrument::all())
													->with('instruments_list', Auth::user()->instruments()->lists('instrument_id'))
													->with('months', $months);
		}

		public function update() {
			$rules = Config::get('validation.account.update');
			
			$validator = Validator::make(Input::all(), $rules);

			$validator->sometimes('email', 'required|email|unique:users', function($input) {
				return $input->email != Auth::user()->email;
			});

			if($validator->fails()) return Redirect::back()->withErrors($validator);

			$instrumentKeys = array();

			if(Input::has('instruments')) { 
				foreach(Input::get('instruments') as $id => $on) {
					if($on == 'on') $instrumentKeys[] = $id;
				}
			}

			$user = Auth::user();
			$user->instruments()->sync($instrumentKeys);

			foreach(['first_name', 'last_name', 'nickname', 'email'] as $str) {
				$user->{$str} = Input::get($str);
			}

			$dateString = '';

			foreach(Input::only(['date', 'month', 'year']) as $key) {
				$dateString .= $key . ' ';
			}

			$user->date_of_birth = Time::parse($dateString);

			Event::fire('user.account-update', array($user));
			$user->save();


			return Redirect::back()->with('success', trans('messages.account-updated'));
		}

		public function updateProfileImage() {
			$validator = Validator::make(Input::all(), Config::get('validation.avatar.update'));

			if($validator->fails()) return Redirect::back()->withErrors($validator);

			try {
				$img = ImageUploader::upload(Input::file('profile_image'));
			} catch(Exception $e) {
				return Redirect::back()->with('error', trans('messages.unexpected-error'));
				// Something went really wrong
			}

			if(Auth::user()->avatar_url) {
				try {
					unlink(Config::get('app.image_upload_path') . Auth::user()->avatar_url); // Delete old one - trash!
				} catch(ErrorException $e) {
					// Do nothing, the image doesn't exist
				}
			}
			Auth::user()->avatar_url = $img->getPath();
			Auth::user()->save();

			Event::fire('user.avatar-update', array(Auth::user()));

			return Redirect::back()->with('success', trans('messages.avatar-updated'));
		}

		public function updatePassword() {
			$validator = Validator::make(Input::all(), Config::get('validation.password.update'));

			if($validator->fails()) return Redirect::back()->withErrors($validator);
			if(!Hash::check(Input::get('current_password'), Auth::user()->password)) return Redirect::back()->withErrors([ 'current_password' => trans('messages.invalid-password')]);

			Auth::user()->password = Hash::make(Input::get('password'));
			Auth::user()->save();

			Event::fire('user.password-update', array(Auth::user()));
			return Redirect::back()->with('success', trans('messages.password-updated'));
		}

		public function updateProfile() {
			$user = Auth::user();

			foreach(Input::only('bio', 'interests', 'influences') as $key => $value) {
				$user->{$key} = $value;
			}

			$user->save();

			return Redirect::back()->with('success', trans('messages.profile-updated'));
		}

		public function removeAvatar() {
			if(Auth::user()->avatar_url) unlink(Config::get('app.image_upload_path') . Auth::user()->avatar_url); // Delete old one - trash!
			Auth::user()->avatar_url = null;
			Auth::user()->save();

			Event::fire('user.avatar-update', array(Auth::user()));
			return Redirect::back()->with('success', trans('messages.avatar-updated'));
		}

	}