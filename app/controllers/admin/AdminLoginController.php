<?php

	class AdminLoginController extends BaseController {

		public function index() {
			return View::make('admin.login.index');
		}

		public function store() {
			/**
			 * If they give bad parameters
			 * If a user exists with the email they supplied, store a failed login attempt
			 */
			if(!Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password')))) {
				if($user = User::whereEmail(Input::get('email'))->first()) {
					Event::fire('user.failed-login', $user);
				}
				return Redirect::back()->with('error', trans('messages.invalid-login-credentials'))->withInput();
			}

			Event::fire('user.login', array(Auth::user()));
			return Redirect::route('dash')->with('success', trans('messages.successful-login'));
		}

		public function logout() {
			Event::fire('user.logout', array(Auth::user()));
			Auth::logout();
			Session::flush();
			return Redirect::to('admin/login')->with('success', trans('messages.signed-out'));
		}

		public function displayForgotten() {
			return View::make('admin.login.forgotten');
        }

		public function submitForgotten() {
			$rules = Config::get('validation.forgotten.submit');

			$validator = Validator::make(Input::all(), $rules);

			if($validator->fails()) return Redirect::to('admin/login/forgotten')->withErrors($validator);

			$user = User::whereEmail(Input::get('email'))->first();

			$token = ResetToken::generate($user);

			Mail::send('emails.auth.reminder', array('token' => $token->token), function($msg) use($user) {
				$msg->subject('Pelugion - Password Reset Request');
				$msg->to($user->email);
			});

			Event::fire('user.request-password-reset', $user);

			//send email
			
			return Redirect::to('admin/login')->with('success', trans('messages.reset-sent'));
		}

		public function resetPassword($token) {
			if(!$token = ResetToken::whereToken($token)->first()) return Redirect::to('admin/login/forgotten')->with('error', trans('messsages.invalid-reset-token'));

			$user = $token->user;
			
			$newPassword = Str::random('10');

			$user->password = Hash::make($newPassword);
			$user->save();

			Mail::send('emails.auth.new-password', array('password' => $newPassword), function($message) use($user) {
				$message->subject('Pelugion - New Password');
				$message->to($user->email);
			});

			$token->delete();

			Event::fire('user.reset-password', $user);
			NoticeHandler::create('password-was-reset', 'danger', $user);

			return Redirect::to('admin/login')->with('success', trans('messages.new-password-sent'));
		}

	}