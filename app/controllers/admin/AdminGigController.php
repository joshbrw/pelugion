<?php

/**
 * Class AdminGigController
 */
class AdminGigController extends BaseController {

	/**
	 * @return mixed
	 */
	public function index()
	{
			$upcoming = Gig::with('venue')->upcoming()->orderBy('date_time', 'ASC')->get();
			$past = Gig::with('venue')->past()->get();

			$upcomingArr = [];
			$pastArr = [];


			/**
			 * Loop through upcoming and past and sort out the image caches
			 */
			foreach($upcoming as &$gig) {
				$gig->img = Config::get('app.image_display_path') . $gig->image_url;
			}

			foreach($past as &$gig) {
				$gig->img = Config::get('app.image_display_path') . $gig->image_url;
			}

			return View::make('admin.gigs.index', [
				'upcomingGigs' => $upcoming,
				'pastGigs' => $past
			]);
		}

	/**
	 * @return mixed
	 */
	public function create()
	{
		Session::forget('gig');

		$venues = [];

		foreach(Venue::all() as $venue) {
			$venues[] = [
				'id' => $venue->id,
				'value' => $venue->name,
				'label' => $venue->name,
				'image' => $venue->getPhotoUrl(),
				'address' => $venue->getAddressString()
			];
		}

		return View::make('admin.gigs.create')->with(compact('venues'));
	}

	/**
	 * @return mixed
	 */
	public function stepTwo()
	{
		if(Session::has('gig.create.venue')) {
			/**
			 * Handle it existing in the session, dunno
			 */
		}
		elseif(Input::get('choose-or-new') == 'new') {
			$validator = Validator::make(Input::get('new-venue'), Config::get('validation.gig.create.venue-info'));

			if($validator->fails()) {
				return Redirect::back()->withInput()->withErrors($validator);
			}
			/**
			 * New venue
			 */

			$venue = new Venue;
			$venue->name = Input::get('new-venue.name');
			$venue->addr_first_line = Input::get('new-venue.address.first-line');
			$venue->addr_second_line = Input::get('new-venue.address.second-line');
			$venue->addr_city = Input::get('new-venue.address.city');
			$venue->addr_county = Input::get('new-venue.address.county');
			$venue->addr_country = (Input::has('new-venue.addr.country')) ? Input::get('new-venue.addr.country') : 'United Kingdom';
			$venue->addr_postcode = Input::get('new-venue.address.postcode');
			$venue->website = Input::get('new-venue.url');
			$venue->extra_details = Input::get('new-venue.extra-details');

			/**
			 * Handle image
			 */

			if(Input::hasFile('new-venue.image')) {
				$file = Input::file('new-venue.image');
				$path = substr(md5(time() . $file->getClientOriginalName()), 10) . '.' . $file->getClientOriginalExtension();

				$img = Image::make($file->getRealPath());

				$img->resize(200, null, true);
				$img->crop(200, 200);

				try {
					$img->save(Config::get('app.image_upload_path') . $path);
				} catch (Exception $e) {
					/**
					 * Who broke the world?
					 */
					return Redirect::back()->with('error', trans('messages.unexpected-error'));
				}

				/**
				 * Use $path to store that shit
				 */

				$venue->avatar_url = $path;
			}

			$venue->save();

			Session::put('gig.create.venue', $venue->toArray());

		} elseif(Input::get('choose-or-new') == 'choose') {
			if(!$venue = Venue::find(Input::get('chosen'))) return Redirect::back()->WithErrors([ trans('messages.object-not-found', ['object' => 'Venue']) ]);

			Session::put('gig.create.venue', $venue->toArray());
		}
		else {
			return Redirect::route('admin.gigs.create')->withInput()->withErrors([ trans('messages.unexpected-error') ]);
		}

		$venue = Venue::find(Session::get('gig.create.venue.id'));

		return View::make('admin.gigs.create-step-two')->with('venue', $venue);
	}

	/**
	 * @return mixed
	 */
	public function stepThree()
	{
		/**
		 * If no session venue stored, fok u
		 */
		if(!Session::has('gig.create.venue')) return Redirect::route('admin.gigs.reset-form');

		if(!$venue = Venue::find(Session::get('gig.create.venue.id'))) return Redirect::route('admin.gigs.create')->with('error', trans('messages.unexpected-error'));

		$validator = Validator::make(Input::get('gig'), array(
			'name' => 'required|max:100',
			'description' => 'required|max:1000',
			'date-time' => 'required|after:now',
			'ticket-price' => 'required|numeric',
		));

		$validator->sometimes('other-age-rating', 'required|max:50', function($input) {
			return Input::get('gig.age-rating') == 'Other';
		});

		if($validator->fails()) {
			return Redirect::back()->withInput()->withErrors($validator);
		}

		$gig = new Gig;
		$gig->name = Input::get('gig.name');
		$gig->description = Input::get('gig.description');
		$gig->age_rating = (Input::get('gig.age-rating') != 'Other') ? Input::get('gig.age-rating') : Input::get('gig.other-age-rating');
		$gig->ticket_price = Input::get('gig.ticket-price');
		$gig->date_time = Time::parse(Input::get('gig.date-time'))->toDateTimeString();

		if(Input::hasFile('gig.image')) {
			$file = Input::file('gig.image');
			$path = substr(md5(time() . $file->getClientOriginalName()), 10) . '.' . $file->getClientOriginalExtension();

			$img = Image::make($file->getRealPath());

			$img->resize(200, null, true);
			$img->crop(200, 200);

			try {
				$img->save(Config::get('app.image_upload_path') . $path);
			} catch (Exception $e) {
				/**
				 * Who broke the world?
				 */
				return Redirect::back()->with('error', trans('messages.unexpected-error'));
			}

			/**
			 * Use $path to store that shit
			 */

			$gig->image_url = $path;
		}

		$gig->venue_id = $venue->id;
		$gig->save();

		Event::fire('gig.create', $gig);

		return View::make('admin.gigs.create-step-three')->with('gig', $gig);
		/**
		 * Unset session as this is the last stepThree
		 */
		Session::forget('gig.create');
	}

	/**
	 * Resets the session and redirects back.
	 */
	public function resetAndRedirect()
	{
		Session::forget('gig.create');

		return Redirect::route('admin.gigs.create');
	}

	/**
	 * @parf(!$am $id
			* @return mixed
		*/
	public function edit($id)
	{
		if(!$gig = Gig::find($id)) return Redirect::back()->with('error', trans('object-not-found', [ 'object' => 'Gig' ]));

		$venueGroupedByCity = Venue::fetchGroupedByCity();
		$returnVenues = [];

		foreach($venueGroupedByCity as $city => $venues) {
			foreach($venues as $venue) {
				$returnVenues[$city][] = [
					'selected' => ($venue->id == $gig->venue_id) ? true : false,
					'id' => $venue->id,
					'name' => $venue->name
				];
			}
		}

		return View::make('admin.gigs.edit')
						->with('gig', $gig)
						->with('venues', $returnVenues);
	}

	/**
	 * @param $id
	 * @return mixed
	 */
	public function update($id) {
		/**
		 * TODO: Reset the image cache if it exists
		 */

		if(!$gig = Gig::find($id)) return Redirect::route('admin.gigs.index')->with('error', trans('messages.object-not-found', ['object' => 'Gig']));

		$validator = Validator::make(Input::all(), array(
			'name' => 'required|max:100',
			'description' => 'required|max:1000',
			'date-time' => 'required|after:now',
			'ticket-price' => 'required|numeric',
		));
		/**
		 * If "age" is set to other make sure to require the other-age-rating parameter
		 */
		$validator->sometimes('other-age-rating', 'required|max:50', function($input) {
			return Input::get('age') == 'Other';
		});

		if($validator->fails()) {
			return Redirect::back()->withInput()->withErrors($validator);
		}

	/**
	 * Validator has passed!
	 */
	$gig->name = Input::get('name');
		$gig->description = Input::get('description');
		$gig->age_rating = (Input::get('age-rating') != 'Other') ? Input::get('age-rating') : Input::get('other-age-rating');
		$gig->ticket_price = Input::get('ticket-price');
		$gig->date_time = Time::parse(Input::get('date-time'))->toDateTimeString();
		$gig->save();

		return Redirect::route('admin.gigs.edit', [ $id ])->with('success', 'That gig has been updated successfully');
	}


	/**
	 * Update a gigs venue, this is sent via AJAX on admin/gigs/edit
	 * @param  string $id Gig ID
	 * @return Response
	 */
	public function updateVenue($id) {
		if(!($gig = Gig::find($id)) || !($venue = Venue::find(Input::get('venueId')))) {
			return Response::make(null, 500);
		}

		$gig->venue_id = $venue->id;
		$gig->save();

		return Response::make(null, 200);
	}

	/**
	 * Update the image of a gig
	 *
	 * @param $gigId	ID of the Gig
	 * @return mixed
	 */
	public function updateImage($gigId)
	{
		try {
			$gig = Gig::findOrFail($gigId);
		} catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
			return Redirect::back()->with('error', trans('messages.object-not-found', ['object' => 'Gig']));
		}

		$rules = [
			'image' => Config::get('validation.misc.image')
		];

		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails()) {
			return Redirect::back()->withErrors($validator);
		}

		try {
			$img = ImageUploader::upload(Input::file('image'));
		} catch(Exception $e) {
			return Redirect::back()->with('error', trans('messages.unexpected-error'));
			// Something went really wrong
		}

		$gig->image_url = $img->getPath();
		$gig->save();

		return Redirect::back()->with('success', 'That gig\'s photo has been updated successfully');
		dd(Input::all());
	}
}