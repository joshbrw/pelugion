<?php

	class AdminDashController extends BaseController {

		public function index() {
			$greeting = \Pelugion\Greeting\Generator::generate();

			return View::make('admin.dash.index')
						->with('greeting', $greeting)
						->with('log_items', LogItem::orderBy('created_at', 'desc')->limit(5)->get())
						->with('count_users', User::all()->count())
						->with('band_members', User::where('band_member', '1')->with('instruments')->get());
						//->with('count_all_posts', Post::all()->count());
		}

	}