<?php

	class AdminUserController extends BaseController {

		public function index() {
			return View::make('admin.users.index')->with('users', User::all());
		}

		public function switchUser($id) {
			if(!$user = User::find($id)) return Redirect::to('admin/users/switch')->with('error', trans('messages.user-not-exist'));
			if(Auth::user() == $user) return Redirect::to('admin/users')->with('error', trans('messages.cant-switch-to-self'));

			Session::put('switched_from', Auth::user()->id);
			Event::fire('user.switch-account', array(Auth::user(), $user));
			Auth::login($user);

			return Redirect::to('admin/dash')->with('success', trans('messages.switched-user'));
		}

		public function switchBack() {
			if(!Session::has('switched_from')) return Redirect::to('admin/dash')->with('error', trans('messages.not-switched'));
			if(!$user = User::find(Session::get('switched_from'))) {
				Auth::logout();
				Session::flush();
				return Redirect::to('admin/login')->with('error', trans('messages.server-error'));
			}

			Session::flush();
			Auth::login($user);
			Event::fire('user.switch-back', $user);

			return Redirect::to('admin/dash')->with('success', trans('messages.switched-back'));
		}

		public function show($id) {
			if(!$user = User::find($id)) return Redirect::route('admin.users.index')->with('error', trans('messages.not-found', ['object'=>'User']));
			
			$last_login = $user->logs()->where('type', 'user.login')->orderBy('created_at', 'desc')->first();
			$log_items = $user->logs()->orderBy('created_at', 'desc');

			$types = array(
				'login' => 'user.login',
				'logout' => 'user.logout',
				'failed-login' => 'user.failed-login',
				'update-account' => 'user.update-account',
				'request-password-reset' => 'user.request-password-reset',
				'post-news' => 'news.post',
				'publish-news' => 'news.publish',
				'unpublish-news' => 'news.unpublish'
			);
			if(Input::has('filter')) {
				$filter = Input::get('filter');
				if(array_key_exists($filter, $types)) {
					$log_items = $log_items->where('type', $types[$filter]);
				}
			} 

			$log_items = $log_items->paginate(10);
			return View::make('admin.users.show')->with('user', $user)->with('last_login', $last_login)->with('log_items', $log_items);
		}

		public function edit($id) {
			if(!$user = User::find($id)) return Redirect::route('admin.users.index')->with('error', trans('messages.not-found', ['object'=>'User']));
			
			return View::make('admin.users.edit')->with('user', $user)
												->with('instruments', Instrument::all())
												->with('instruments_list', $user->instruments()->lists('instrument_id'));
		}

		public function update($id) {
			if(!$user = User::find($id)) return Redirect::route('admin.users.index')->r('error', trans('messages.not-found', ['object'=>'User']));

			$rules = array(
				'first_name' => 'required|max:50',
				'last_name' => 'required|max:50',
				'email' => 'required|email',
				'role' => 'required'
			);

			if(Input::get('email') != $user->email) {
				$rules['email'] = 'required|email|unique:users';
			}
			
			$validator = Validator::make(Input::all(), $rules);

			$validator->sometimes('password_confirm', 'required|same:password', function($input) {
				return $input->password != null;
			});

			if($validator->fails()) return Redirect::back()->withErrors($validator);

			$instrumentKeys = array();

			if(Input::has('instruments')) { 
				foreach(Input::get('instruments') as $id => $on) {
					if($on == 'on') $instrumentKeys[] = $id;
				}
			}

			$user->instruments()->sync($instrumentKeys);
			$user->first_name = Input::get('first_name');
			$user->last_name = Input::get('last_name');
			$user->email = Input::get('email');


			if(Input::get('band_member') == 'on')
				$user->band_member = 1;
			else
				$user->band_member = 0;
				
			if(Input::get('role') == 'Super Administrator') {
				$user->role = 1;
			} else {
				$user->role = 0;
			}

			if(Input::has('password') && Input::has('password_confirm')) $user->password = Hash::make(Input::get('password'));

			$user->save();
			Event::fire('user.update-other-account', array(Auth::user(), $user));


			return Redirect::to("admin/users")->with('success', trans('messages.account-updated'));
		}

		public function updateSpecific($id) {
			if(!$user = User::find($id)) dd('user not found');
			$data = Input::all();

			switch($data['id']) {
				case 'full_name':
					$split = explode(' ', $data['value']);
					$user->first_name = $split[0];
					$user->last_name = $split[1];
					$user->save();
					break;

				case 'date_of_birth':
					$dob = Time::parse($data['value']);
					$user->date_of_birth = $dob;
					$user->save();

					return Time::parse($user->date_of_birth)->day . '-' . Time::parse($user->date_of_birth)->month . '-' . Time::parse($user->date_of_birth)->year;
					break;

				default:
					$user->{$data['id']} = $data['value'];
					$user->save();
					break;
			}
			$user->save();

			return $user->{$data['id']};
		}
	}