<?php

class AdminContentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$pages = Page::with('contentSections')->get();

		return View::make('admin.content.index', compact('pages'));
	}

	/**
	 * Update a specific content section. This is called from the content.index page update buttons
	 *
	 * @return Response
	 */
	
	public function updateSection() {
		try {
			$section = ContentSection::where('page_id', Input::get('pageId'))->findOrFail(Input::get('sectionId'));
		}
		catch(Exception $e) {
			return Response::json([
				'success' => false,
				'message' => 'There was an error with the Page or Section ID'
			]);
		}


		/**
		 * Check that the page has the section
		 */
		
		$section->content = Input::get('content');
		$section->save();
		
		return Response::json([
			'success' => true,
		]);

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}