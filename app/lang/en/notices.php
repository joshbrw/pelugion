<?php

	return array(
		'password-was-reset' => 'Your password was recently reset. Please ensure you <a href="' . url('admin/account#change-password') . '">change your password.</a>',
	);