<?php

	return array(
		'invalid-login-credentials' => 'Please check your login credentials.',
		'signed-out' => 'You have successfully signed out.', 
		'successful-login' => 'You have successfully signed in.',
		'not-super-admin' => 'You do not have the privelidges for that.',
		'not-found' => 'That :object doesn\'t exist.',
		'not-switched' => 'You had not switched from a user.',
		'switched-user' => 'You have successfully switched user.',
		'switched-back' => 'You have successfully switched back.',
		'cant-switch-to-self' => 'You cannot switch to yourself.',
		'reset-sent' => 'A reset link has been sent to your email. If you cannot find it please check your spam inbox.',
		'invalid-reset-token' => 'That reset token is invalid or expired. Please send another one.',
		'new-password-sent' => 'A new password has been sent to your email address.',
		'not-authorized' => 'You are not authorized to perform that action.',
		'server-error' => 'A server error occured. Please log in again.',

		'account-updated' => 'Your account has been succesfully updated.',
		'avatar-updated' => 'Your avatar has successfully been updated.',
		'password-updated' => "Your password has successfully been updated.",
		'profile-updated' => 'Your profile has successfully been updated.',

		'news-published' => 'That news story has now been published and will be visible.',
		'news-unpublished' => 'That news story has now been unpublished and is not available to view on the website.',

		'all-news-deleted' => 'All news has been successfully deleted.',

		'object-not-found' => 'That :object could not be found',
		'object-created' => 'That :object has now been created.',
		'object-deleted' => 'That :object has now been deleted.',
		'object-updated' => 'That :object has been updated.',

		'unexpected-error' => 'An unexpected error occured. Please try again.',

		'no-news' => 'There are currently no news stories.',

		// Used when updating profile
		'invalid-password' => 'The current password field does not match your current password.',


		'invalid-captcha' => 'Incorrect Captcha. Please try again.',
	);