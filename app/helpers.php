<?php

	if(!function_exists('shorten')) {
		function shorten($string, $length = 10) {
			if (strlen($string) <= $length) {
				return $string;
			}

			$string = $string . ' ';
			$string = substr($string, 0, $length);
			$string = substr($string, 0, strrpos($string, ' '));

			return $string . '...';
		}
	}

	if(!function_exists('get_content')) {
		function get_content($string) {
			return ContentSection::findSection($string);
		}
	}