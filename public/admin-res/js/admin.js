$(document).ready(function() {
	$('.clickable').css( 'cursor', 'pointer' );
	$('.clickable').click(function() {
		window.location = $(this).attr('href');
	});
	$('.edit').each(function() {
    	$(this).editable('http://pelugion.dev/admin/users/' + $(this).attr('data-id') + '/updateSpecific', {
	    	indicator: 'Saving...',
	        tooltip   : 'Click to edit...',
	        callback: function(value, settings) {
	        	$('*[data-field=' + $(this).attr('id') + ']').html(value);
	        }
	    });		
	});
	$('.confirm-click').click(function(e) {
		if(confirm('Are you sure?') == false)
			return false; 
	});
});